<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Agrekom\ProductConfigurator\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface CustomerConfigurationRepositoryInterface
{

    /**
     * Save CustomerConfiguration
     * @param \Agrekom\ProductConfigurator\Api\Data\CustomerConfigurationInterface $customerConfiguration
     * @return \Agrekom\ProductConfigurator\Api\Data\CustomerConfigurationInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Agrekom\ProductConfigurator\Api\Data\CustomerConfigurationInterface $customerConfiguration
    );

    /**
     * Retrieve CustomerConfiguration
     * @param string $customerconfigurationId
     * @return \Agrekom\ProductConfigurator\Api\Data\CustomerConfigurationInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($customerconfigurationId);

    /**
     * Retrieve CustomerConfiguration matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Agrekom\ProductConfigurator\Api\Data\CustomerConfigurationSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete CustomerConfiguration
     * @param \Agrekom\ProductConfigurator\Api\Data\CustomerConfigurationInterface $customerConfiguration
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Agrekom\ProductConfigurator\Api\Data\CustomerConfigurationInterface $customerConfiguration
    );

    /**
     * Delete CustomerConfiguration by ID
     * @param string $customerconfigurationId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($customerconfigurationId);
}


<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Agrekom\ProductConfigurator\Api\Data;

interface CustomerConfigurationInterface
{

    const PRODUCT_ID = 'product_id';
    const CUSTOMER_ID = 'customer_id';
    const CUSTOMERCONFIGURATION_ID = 'customerconfiguration_id';
    const CONFIGURATION_ID = 'configuration_id';

    /**
     * Get customerconfiguration_id
     * @return string|null
     */
    public function getCustomerconfigurationId();

    /**
     * Set customerconfiguration_id
     * @param string $customerconfigurationId
     * @return \Agrekom\ProductConfigurator\CustomerConfiguration\Api\Data\CustomerConfigurationInterface
     */
    public function setCustomerconfigurationId($customerconfigurationId);

    /**
     * Get product_id
     * @return string|null
     */
    public function getProductId();

    /**
     * Set product_id
     * @param string $productId
     * @return \Agrekom\ProductConfigurator\CustomerConfiguration\Api\Data\CustomerConfigurationInterface
     */
    public function setProductId($productId);

    /**
     * Get configuration_id
     * @return string|null
     */
    public function getConfigurationId();

    /**
     * Set configuration_id
     * @param string $configurationId
     * @return \Agrekom\ProductConfigurator\CustomerConfiguration\Api\Data\CustomerConfigurationInterface
     */
    public function setConfigurationId($configurationId);

    /**
     * Get customer_id
     * @return string|null
     */
    public function getCustomerId();

    /**
     * Set customer_id
     * @param string $customerId
     * @return \Agrekom\ProductConfigurator\CustomerConfiguration\Api\Data\CustomerConfigurationInterface
     */
    public function setCustomerId($customerId);
}


<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Agrekom\ProductConfigurator\Api\Data;

interface CustomerConfigurationSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get CustomerConfiguration list.
     * @return \Agrekom\ProductConfigurator\Api\Data\CustomerConfigurationInterface[]
     */
    public function getItems();

    /**
     * Set product_id list.
     * @param \Agrekom\ProductConfigurator\Api\Data\CustomerConfigurationInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}


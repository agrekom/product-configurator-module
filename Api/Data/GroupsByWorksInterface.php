<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Agrekom\ProductConfigurator\Api\Data;

interface GroupsByWorksInterface
{

    const GROUP_ID = 'group_id';
    const ID = 'id';
    const CODE = 'code';
    const TITLE = 'title';

    /**
     * Get id
     * @return string|null
     */
    public function getId();

    /**
     * Set id
     * @param string $id
     * @return \Agrekom\ProductConfigurator\GroupsByWorks\Api\Data\GroupsByWorksInterface
     */
    public function setId($id);

    /**
     * Get code
     * @return string|null
     */
    public function getCode();

    /**
     * Set code
     * @param string $code
     * @return \Agrekom\ProductConfigurator\GroupsByWorks\Api\Data\GroupsByWorksInterface
     */
    public function setCode($code);

    /**
     * Get title
     * @return string|null
     */
    public function getTitle();

    /**
     * Set title
     * @param string $title
     * @return \Agrekom\ProductConfigurator\GroupsByWorks\Api\Data\GroupsByWorksInterface
     */
    public function setTitle($title);

    /**
     * Get group_id
     * @return string|null
     */
    public function getGroupId();

    /**
     * Set group_id
     * @param string $groupId
     * @return \Agrekom\ProductConfigurator\GroupsByWorks\Api\Data\GroupsByWorksInterface
     */
    public function setGroupId($groupId);
}


<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Agrekom\ProductConfigurator\Api\Data;

interface GroupsByWorksSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get GroupsByWorks list.
     * @return \Agrekom\ProductConfigurator\Api\Data\GroupsByWorksInterface[]
     */
    public function getItems();

    /**
     * Set code list.
     * @param \Agrekom\ProductConfigurator\Api\Data\GroupsByWorksInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}


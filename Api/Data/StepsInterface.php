<?php

namespace Agrekom\ProductConfigurator\Api\Data;

interface StepsInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const CREATED_AT = 'created_at';
    const STEPS_ID = 'steps_id';
    const CODE = 'code';
    const TITLE = 'title';
    const PAGE_LAYOUT = 'page_layout';
    const PURPOSE_PRODUCT_ID = 'purpose_product_id';
    const COMPARISION_TABLE_TITLE = 'comparison_table_title';

    /**
     * Get comparison_table_title
     * @return string|null
     */
    public function getComparisonTableTitle();

    /**
     * Set comparison_table_title
     * @param string $comparisonTableTitle
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsInterface
     */
    public function setComparisonTableTitle(int $comparisonTableTitle);

    /**
     * Get purpose_product_id
     * @return int|null
     */
    public function getPurposeProductId();

    /**
     * Set purpose_product_id
     * @param int $purposeProductId
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsInterface
     */
    public function setPurposeProductId(int $purposeProductId);

    /**
     * Get page_layout
     * @return string|null
     */
    public function getPageLayout();

    /**
     * Set page_layout
     * @param string $pageLayout
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsInterface
     */
    public function setPageLayout($pageLayout);

    /**
     * Get steps_id
     * @return string|null
     */
    public function getStepsId();

    /**
     * Set steps_id
     * @param string $stepsId
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsInterface
     */
    public function setStepsId($stepsId);

    /**
     * Get title
     * @return string|null
     */
    public function getTitle();

    /**
     * Set title
     * @param string $title
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsInterface
     */
    public function setTitle($title);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Agrekom\ProductConfigurator\Api\Data\StepsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Agrekom\ProductConfigurator\Api\Data\StepsExtensionInterface $extensionAttributes
    );

    /**
     * Get code
     * @return string|null
     */
    public function getCode();

    /**
     * Set code
     * @param string $code
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsInterface
     */
    public function setCode($code);

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsInterface
     */
    public function setCreatedAt($createdAt);
}


<?php

namespace Agrekom\ProductConfigurator\Api\Data;

interface StepsSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Steps list.
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsInterface[]
     */
    public function getItems();

    /**
     * Set title list.
     * @param \Agrekom\ProductConfigurator\Api\Data\StepsInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}


<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Agrekom\ProductConfigurator\Api\Data;

interface WiresSkuCombinationInterface
{

    const ID = 'id';
    const SKU = 'sku';
    const SKU_COMBINATION = 'sku_combination';

    /**
     * Get id
     * @return string|null
     */
    public function getId();

    /**
     * Set id
     * @param string $id
     * @return \Agrekom\ProductConfigurator\WiresSkuCombination\Api\Data\WiresSkuCombinationInterface
     */
    public function setId($id);

    /**
     * Get sku
     * @return string|null
     */
    public function getSku();

    /**
     * Set sku
     * @param string $sku
     * @return \Agrekom\ProductConfigurator\WiresSkuCombination\Api\Data\WiresSkuCombinationInterface
     */
    public function setSku($sku);

    /**
     * Get sku_combination
     * @return string|null
     */
    public function getSkuCombination();

    /**
     * Set sku_combination
     * @param string $skuCombination
     * @return \Agrekom\ProductConfigurator\WiresSkuCombination\Api\Data\WiresSkuCombinationInterface
     */
    public function setSkuCombination($skuCombination);
}


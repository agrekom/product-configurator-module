<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Agrekom\ProductConfigurator\Api\Data;

interface WiresSkuCombinationSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get WiresSkuCombination list.
     * @return \Agrekom\ProductConfigurator\Api\Data\WiresSkuCombinationInterface[]
     */
    public function getItems();

    /**
     * Set sku list.
     * @param \Agrekom\ProductConfigurator\Api\Data\WiresSkuCombinationInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}


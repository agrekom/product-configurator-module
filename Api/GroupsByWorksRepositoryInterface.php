<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Agrekom\ProductConfigurator\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface GroupsByWorksRepositoryInterface
{

    /**
     * Save GroupsByWorks
     * @param \Agrekom\ProductConfigurator\Api\Data\GroupsByWorksInterface $groupsByWorks
     * @return \Agrekom\ProductConfigurator\Api\Data\GroupsByWorksInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Agrekom\ProductConfigurator\Api\Data\GroupsByWorksInterface $groupsByWorks
    );

    /**
     * Retrieve GroupsByWorks
     * @param string $id
     * @return \Agrekom\ProductConfigurator\Api\Data\GroupsByWorksInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($id);

    /**
     * Retrieve GroupsByWorks matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Agrekom\ProductConfigurator\Api\Data\GroupsByWorksSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete GroupsByWorks
     * @param \Agrekom\ProductConfigurator\Api\Data\GroupsByWorksInterface $groupsByWorks
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Agrekom\ProductConfigurator\Api\Data\GroupsByWorksInterface $groupsByWorks
    );

    /**
     * Delete GroupsByWorks by ID
     * @param string $id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id);
}


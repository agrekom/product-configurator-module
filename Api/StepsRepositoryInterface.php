<?php

namespace Agrekom\ProductConfigurator\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface StepsRepositoryInterface
{

    /**
     * Save Steps
     * @param \Agrekom\ProductConfigurator\Api\Data\StepsInterface $steps
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Agrekom\ProductConfigurator\Api\Data\StepsInterface $steps
    );

    /**
     * Retrieve Steps
     * @param string $stepsId
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($stepsId);

    /**
     * Retrieve Steps matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Steps
     * @param \Agrekom\ProductConfigurator\Api\Data\StepsInterface $steps
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Agrekom\ProductConfigurator\Api\Data\StepsInterface $steps
    );

    /**
     * Delete Steps by ID
     * @param string $stepsId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($stepsId);
}


<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Agrekom\ProductConfigurator\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface WiresSkuCombinationRepositoryInterface
{

    /**
     * Save WiresSkuCombination
     * @param \Agrekom\ProductConfigurator\Api\Data\WiresSkuCombinationInterface $wiresSkuCombination
     * @return \Agrekom\ProductConfigurator\Api\Data\WiresSkuCombinationInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Agrekom\ProductConfigurator\Api\Data\WiresSkuCombinationInterface $wiresSkuCombination
    );

    /**
     * Retrieve WiresSkuCombination
     * @param string $id
     * @return \Agrekom\ProductConfigurator\Api\Data\WiresSkuCombinationInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($id);

    /**
     * Retrieve WiresSkuCombination matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Agrekom\ProductConfigurator\Api\Data\WiresSkuCombinationSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete WiresSkuCombination
     * @param \Agrekom\ProductConfigurator\Api\Data\WiresSkuCombinationInterface $wiresSkuCombination
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Agrekom\ProductConfigurator\Api\Data\WiresSkuCombinationInterface $wiresSkuCombination
    );

    /**
     * Delete WiresSkuCombination by ID
     * @param string $id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id);
}


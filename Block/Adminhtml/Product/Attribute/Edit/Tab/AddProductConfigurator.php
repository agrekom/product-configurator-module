<?php

namespace Agrekom\ProductConfigurator\Block\Adminhtml\Product\Attribute\Edit\Tab;

class AddProductConfigurator extends \Magento\Backend\Block\Widget\Form\Generic
{

    /**
     * @var \Magento\Config\Model\Config\Source\Yesno
     */
    protected $yesNo;

    /**
     * @var \Magento\Eav\Block\Adminhtml\Attribute\PropertyLocker
     */
    private $propertyLocker;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Eav\Block\Adminhtml\Attribute\PropertyLocker $propertyLocker
     * @param \Magento\Config\Model\Config\Source\Yesno $yesNo
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Eav\Block\Adminhtml\Attribute\PropertyLocker $propertyLocker,
        \Magento\Config\Model\Config\Source\Yesno $yesNo,
        array $data = []
    )
    {
        $this->yesNo = $yesNo;
        $this->propertyLocker = $propertyLocker;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD)
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );

        $fieldset = $form->addFieldset(
            'product_configurator_fieldset',
            ['legend' => __('Options'), 'collapsable' => false]
        );

        $fieldset->addField(
            'use_in_product_configurator',
            'select',
            [
                'name' => 'use_in_product_configurator',
                'label' => __('Use this attribute in product configurator'),
                'title' => __('Use this attribute in product configurator'),
                'values' => $this->yesNo->toOptionArray()
            ]
        );

        $this->setForm($form);
        $this->propertyLocker->lock($form);

        return $this;
    }

    /**
     * Retrieve attribute object from registry
     *
     * @return mixed
     */
    private function getAttributeObject()
    {
        return $this->_coreRegistry->registry('entity_attribute');
    }

    /**
     * Initialize form fields values
     *
     * @return $this
     */
    protected function _initFormValues()
    {
        $this->getForm()->addValues($this->getAttributeObject()->getData());
        return parent::_initFormValues();
    }

}

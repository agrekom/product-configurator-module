<?php

namespace Agrekom\ProductConfigurator\Block\Checkout\Cart\Item\Renderer\Actions;

class Remove extends \Magento\Checkout\Block\Cart\Item\Renderer\Actions\Remove
{

    /**
     * @return string|null
     */
    public function getCustomerConfigurationId()
    {
        return $this->getItem()->getData('customer_configuration_id');
    }

}

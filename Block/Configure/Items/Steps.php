<?php

namespace Agrekom\ProductConfigurator\Block\Configure\Items;

use Agrekom\ProductConfigurator\Api\GroupsByWorksRepositoryInterface;

class Steps extends \Magento\Framework\View\Element\Template
{

    /**
     * @var \Agrekom\ProductConfigurator\Api\GroupsByWorksRepositoryInterface
     */
    protected $groupsByWorksRepository;

    /**
     * @var \Agrekom\ProductConfigurator\Api\StepsRepositoryInterface
     */
    protected $stepsRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Magento\Framework\Api\SortOrderBuilder
     */
    protected $sortOrderBuilder;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Magento\Catalog\Helper\Data
     */
    protected $catalogDataHelper;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    protected $pricingDataHelper;

    /**
     * @var \Magento\Catalog\Helper\Image
     */
    protected $catalogImageHelper;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Eav\Attribute
     */
    protected $attributeFactory;

    /**
     * @var \Agrekom\ProductConfigurator\Model\Dependent
     */
    protected $dependentProducts;

    /**
     * @var \Agrekom\ProductConfigurator\Model\Accessory
     */
    protected $productAccessories;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $product;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    public function __construct(
        \Agrekom\ProductConfigurator\Api\GroupsByWorksRepositoryInterface $groupsByWorksRepository,
        \Agrekom\ProductConfigurator\Api\StepsRepositoryInterface $stepsRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\SortOrderBuilder $sortOrderBuilder,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        \Magento\Catalog\Helper\Data $catalogDataHelper,
        \Magento\Framework\Pricing\Helper\Data $pricingDataHelper,
        \Magento\Catalog\Helper\Image $catalogImageHelper,
        \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attributeFactory,
        \Agrekom\ProductConfigurator\Model\Dependent $dependentProducts,
        \Agrekom\ProductConfigurator\Model\Accessory $productAccessories,
        \Magento\Catalog\Model\Product $product,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    )
    {
        $this->groupsByWorksRepository = $groupsByWorksRepository;
        $this->stepsRepository = $stepsRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
        $this->collectionFactory = $collectionFactory;
        $this->catalogDataHelper = $catalogDataHelper;
        $this->pricingDataHelper = $pricingDataHelper;
        $this->catalogImageHelper = $catalogImageHelper;
        $this->attributeFactory = $attributeFactory;
        $this->dependentProducts = $dependentProducts;
        $this->productAccessories = $productAccessories;
        $this->product = $product;
        $this->productFactory = $productFactory;
        $this->logger = $logger;

        parent::__construct($context, $data);
    }

    /**
     * @return array|null
     */
    public function getGroupsByWorks()
    {
        try {
            $searchCriteria = $this->searchCriteriaBuilder->addFilter(
                \Agrekom\ProductConfigurator\Api\Data\GroupsByWorksInterface::CODE,
                'none',
                'neq'
            )->create();

            $groupsByWorks = $this->groupsByWorksRepository->getList($searchCriteria)->getItems();
            $result = [];
            foreach ($groupsByWorks as $groupsByWork) {
                $result[] = [
                    'group' => $groupsByWork->getGroupId(),
                    'code' => $groupsByWork->getCode()
                ];
            }

            return json_encode($result);
        } catch (\Magento\Framework\Exception\LocalizedException $localizedException) {
            return null;
        }
    }

    /**
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsInterface[]|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function prepareSteps()
    {
        try {
            $sortOrder = $this->sortOrderBuilder->setField('code')->setDirection('ASC')->create();
            $searchCriteria = $this->searchCriteriaBuilder
                ->addSortOrder($sortOrder)->create();
            $steps = $this->stepsRepository->getList($searchCriteria);

            if (!$steps->getTotalCount()) {
                return null;
            }

            $result['step_10'] = [
                'step_title' => __('Purpose'),
                'step_page_layout' => 'purpose',
                'step_products' => $this->prepareStepProducts('', 1),
                'step_attribute_codes' => null,
                'dependent_from_purpose_product_id' => 0,
                'comparison_table_title' => 'comparison_table'
            ];

            /** @var \Agrekom\ProductConfigurator\Api\Data\StepsInterface $step */
            foreach ($steps->getItems() as $step) {
                $stepProducts = $this->prepareStepProducts($step->getCode());
                if (!$stepProducts) {
                    continue;
                }
                $result[$step->getCode()] = [
                    'step_title' => __($step->getTitle()),
                    'step_page_layout' => $step->getPageLayout(),
                    'step_products' => $stepProducts,
                    'step_attribute_codes' => $this->prepareStepAtttributeCodes($stepProducts),
                    'dependent_from_purpose_product_id' => $step->getPurposeProductId(),
                    'comparison_table_title' => $step->getComparisonTableTitle()
                ];
            }

            ksort($result, SORT_NATURAL);

            $result['summary_step'] = [
                'step_title' => __('Summary'),
                'step_page_layout' => 'summary',
                'step_products' => null,
                'step_attribute_codes' => null,
                'dependent_from_purpose_product_id' => 0,
                'comparison_table_title' => 'comparison_table'
            ];

            return $result;
        } catch (\Magento\Framework\Exception\LocalizedException $localizedException) {
            $this->logger->error($localizedException->getMessage());
        }

        return null;
    }

    /**
     * @param $productsCollection
     * @return array|null
     */
    public function prepareStepAtttributeCodes($products)
    {
        $attributeCodes = [];
        /** @var \Magento\Catalog\Model\Product $product */
        foreach ($products as $item) {
            $product = $this->product->loadByAttribute('sku', $item['sku']);
            $attributes = $product->getAttributes();
            foreach ($attributes as $attribute) {
                if ($attribute->getData('use_in_product_configurator')) {
                    $attributeCodes[$attribute->getAttributeCode()] = $attribute->getStoreLabel();
                }
            }
        }

        return (!empty($attributeCodes)) ? $attributeCodes : null;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return array|null
     */
    public function prepareStepProductAttributes(\Magento\Catalog\Model\Product $product)
    {
        $data = [];
        $attributes = $product->getAttributes();
        foreach ($attributes as $attribute) {
            if ($attribute->getData('use_in_product_configurator')) {
                $value = $attribute->getFrontend()->getValue($product);

                if ($value instanceof \Magento\Framework\Phrase) {
                    $value = (string) htmlspecialchars($value->render());
                } elseif ($attribute->getFrontendInput() == 'price' && is_string($value)) {
                    $value = $this->priceCurrency->convertAndFormat($value);
                }

                if (is_string($value) && strlen(trim($value))) {
                    $data[$attribute->getAttributeCode()] = [
                        'label' => htmlspecialchars($attribute->getStoreLabel()),
                        'value' => htmlspecialchars($value),
                        'code' => $attribute->getAttributeCode(),
                    ];
                }
            }
        }

        return (count($data)) ? $data : null;
    }

    /**
     * @param string $stepCode
     * @param int $purpose
     * @return array|null
     */
    public function prepareStepProducts(string $stepCode, $purpose = 0)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $products */
        $products = $this->collectionFactory->create();
        $products->addAttributeToSelect('*');
        if ($purpose) {
            $products->addAttributeToFilter('is_purpose_product', 1);
        } else {
            $products->addAttributeToFilter('product_configurator_step', ['finset' => [$stepCode]]);
        }

        $products->setOrder('product_configurator_sort_order', \Magento\Framework\DB\Select::SQL_ASC);

        if (!$products->count()) {
            return null;
        }

        $result = [];
        /** @var \Magento\Catalog\Model\Product $product */
        foreach ($products as $product) {
            $price = $this->catalogDataHelper->getTaxPrice($product, $product->getFinalPrice(), false);
            $priceWithTax = $this->catalogDataHelper->getTaxPrice($product, $product->getFinalPrice(), true);

            $dependentProductIds = [];
            $dependentProductsLinkCollection = $this->dependentProducts->getDependentLinkCollection($product);
            /** @var \Magento\Catalog\Model\Product\Link $dependentLink */
            foreach ($dependentProductsLinkCollection as $dependentLink) {
                $dependentLinkedProductId = $dependentLink->getLinkedProductId();
                $dependentProductIds[$dependentLinkedProductId] = $dependentLinkedProductId;
            }

            $accessoryProducts = [];
            $accessoryProductsLinkCollection = $this->productAccessories->getAccessoryLinkCollection($product);
            /** @var \Magento\Catalog\Model\Product\Link $accessoryLink */
            foreach ($accessoryProductsLinkCollection as $accessoryLink) {
                $accessoryLinkedProductId = $accessoryLink->getLinkedProductId();
                $accessoryProducts[$accessoryLinkedProductId] = $this->prepareProductData($accessoryLinkedProductId);
            }

            $productConfiguratorShortDesc = (string) $product->getData('product_configurator_short_desc');
            $productName = (string) $product->getName();

            $result[] = [
                'id' => $product->getId(),
                'sku' => $product->getSku(),
                'name' => htmlspecialchars($productName),
                'short_desc' => htmlspecialchars($productConfiguratorShortDesc),
                'price' => $this->pricingDataHelper->currency($price, true, false),
                'raw_price' => $price,
                'price_with_tax' => $this->pricingDataHelper->currency($priceWithTax,true, false),
                'raw_price_with_tax' => $priceWithTax,
                'image' => $this->catalogImageHelper->init($product, 'product_base_image')->getUrl(),
                'attributes' => $this->prepareStepProductAttributes($product),
                'dependent_product_ids' => $dependentProductIds,
                'product_accessories' => $accessoryProducts,
                'left_col_related_products' => $this->prepareLeftColRelatedProducts($product),
                'group_by_work' => $this->prepareGroupByWork($product),
                'show_only_for_purpose_group' => $product->getData('show_only_for_purpose_group'),
                'is_container_product' => $product->getData('is_container_product'),
                'is_purpose_product' => $product->getData('is_purpose_product'),
                'dlugosc_przewodow_konfigurator' => $product->getData('dlugosc_przewodow_konfigurator'),
                'show_only_if_purpose_product_is_chosen' => $product->getData('show_only_if_purpose_product_is_chosen'),
            ];
        }

        return $result;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return \Agrekom\ProductConfigurator\Api\Data\GroupsByWorksInterface|null
     */
    protected function prepareGroupByWork(\Magento\Catalog\Model\Product $product)
    {
        $workByGroupCode = $product->getData('group_by_work');
        if (!$workByGroupCode) {
            return null;
        }

        try {
            $searchCriteria = $this->searchCriteriaBuilder->addFilter(
                \Agrekom\ProductConfigurator\Api\Data\GroupsByWorksInterface::CODE,
                $workByGroupCode,
                'eq'
            )->create();

            $groupsByWorks = $this->groupsByWorksRepository->getList($searchCriteria);
            if (!$groupsByWorks->getTotalCount()) {
                return null;
            }

            $items = $groupsByWorks->getItems();
            $item = end($items);

            return [
                'group' => $item->getGroupId(),
                'code' => $item->getCode()
            ];
        } catch (\Magento\Framework\Exception\LocalizedException $localizedException) {
            return null;
        }
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return array[]|null
     */
    protected function prepareLeftColRelatedProducts(\Magento\Catalog\Model\Product $product)
    {
        $wiresSkus = $product->getData('dlugosc_przewodow_konfigurator');
        if (!$wiresSkus) {
            return null;
        }
        $wires = [];
        $wiresSkus = explode(",", $wiresSkus);
        foreach ($wiresSkus as $wireSku) {
            $wireSku = trim($wireSku);
            $product = $this->product->loadByAttribute('sku', $wireSku);
            if (!$product) {
                continue;
            }

            $dependentProductIds = [];
            $dependentProductsLinkCollection = $this->dependentProducts->getDependentLinkCollection($product);
            /** @var \Magento\Catalog\Model\Product\Link $dependentLink */
            foreach ($dependentProductsLinkCollection as $dependentLink) {
                $dependentLinkedProductId = $dependentLink->getLinkedProductId();
                $dependentProductIds[$dependentLinkedProductId] = $dependentLinkedProductId;
            }

            $price = $this->catalogDataHelper->getTaxPrice($product, $product->getFinalPrice(), false);
            $priceWithTax = $this->catalogDataHelper->getTaxPrice($product, $product->getFinalPrice(), true);

            $productConfiguratorShortDesc = (string) $product->getData('product_configurator_short_desc');
            $productName = (string) $product->getName();

            $wires[] = [
                'id' => $product->getId(),
                'sku' => $product->getSku(),
                'name' => htmlspecialchars($productName),
                'short_desc' => htmlspecialchars($productConfiguratorShortDesc),
                'price' => $this->pricingDataHelper->currency($price, true, false),
                'raw_price' => $price,
                'price_with_tax' => $this->pricingDataHelper->currency($priceWithTax,true, false),
                'raw_price_with_tax' => $priceWithTax,
                'image' => $this->catalogImageHelper->init($product, 'product_base_image')->getUrl(),
                'dependent_from_purpose_products' => $dependentProductIds
            ];
        }

        return [
            'wires' => $wires,
        ];
    }

    /**
     * @param int $productId
     * @return array
     */
    protected function prepareProductData(int $productId)
    {
        $product = $this->productFactory->create();
        $product->load($productId);
        $price = $this->catalogDataHelper->getTaxPrice($product, $product->getFinalPrice(), false);
        $priceWithTax = $this->catalogDataHelper->getTaxPrice($product, $product->getFinalPrice(), true);
        $productConfiguratorShortDesc = (string) $product->getData('product_configurator_short_desc');
        $productName = (string) $product->getName();

        return [
            'id' => $product->getId(),
            'sku' => $product->getSku(),
            'name' => htmlspecialchars($productName),
            'short_desc' => htmlspecialchars($productConfiguratorShortDesc),
            'price' => $this->pricingDataHelper->currency($price, true, false),
            'raw_price' => $price,
            'price_with_tax' => $this->pricingDataHelper->currency($priceWithTax,true, false),
            'raw_price_with_tax' => $priceWithTax,
            'image' => $this->catalogImageHelper->init($product, 'product_base_image')->getUrl(),
        ];
    }

}

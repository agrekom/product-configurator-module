<?php

namespace Agrekom\ProductConfigurator\Controller\AddToCart;

class Index extends \Magento\Framework\App\Action\Action
{

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $redirectFactory;

    /**
     * @var \Agrekom\ProductConfigurator\Model\AddToCart\AddProductsToCart
     */
    protected $addProductsToCart;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory,
        \Agrekom\ProductConfigurator\Model\AddToCart\AddProductsToCart $addProductsToCart,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->redirectFactory = $redirectFactory;
        $this->addProductsToCart = $addProductsToCart;
        $this->logger = $logger;
        parent::__construct($context);
    }

    public function execute()
    {
        $items = $this->getRequest()->getPost('items');

        try {
            $this->addProductsToCart->execute($items);
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
            $this->messageManager->addErrorMessage(
                __('Sorry but there was a problem when adding products to cart. Please contact us directly at biuro@agrekom.pl')
            );
        }

        $resultRedirect = $this->redirectFactory->create();
        $resultRedirect->setPath('checkout/cart');

        return $resultRedirect;
    }

}

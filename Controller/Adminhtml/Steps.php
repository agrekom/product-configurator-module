<?php

namespace Agrekom\ProductConfigurator\Controller\Adminhtml;

abstract class Steps extends \Magento\Backend\App\Action
{

    const ADMIN_RESOURCE = 'Agrekom_ProductConfigurator::top_level';
    protected $_coreRegistry;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
            ->addBreadcrumb(__('Agrekom'), __('Agrekom'))
            ->addBreadcrumb(__('Product Configurator Steps Management'), __('Product Configurator Steps Management'));

        return $resultPage;
    }
}


<?php

namespace Agrekom\ProductConfigurator\Controller\Adminhtml\Steps;

class Delete extends \Agrekom\ProductConfigurator\Controller\Adminhtml\Steps
{

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('steps_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(\Agrekom\ProductConfigurator\Model\Steps::class);
                $model->load($id);
                $model->delete();
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the steps.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['steps_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find steps to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}


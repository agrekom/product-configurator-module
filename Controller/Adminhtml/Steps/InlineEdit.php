<?php

namespace Agrekom\ProductConfigurator\Controller\Adminhtml\Steps;

class InlineEdit extends \Magento\Backend\App\Action
{

    protected $jsonFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
    ) {
        parent::__construct($context);
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * Inline edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        if ($this->getRequest()->getParam('isAjax')) {
            $postItems = $this->getRequest()->getParam('items', []);
            if (!count($postItems)) {
                $messages[] = __('Please correct the data sent.');
                $error = true;
            } else {
                foreach (array_keys($postItems) as $modelid) {
                    /** @var \Agrekom\ProductConfigurator\Model\Steps $model */
                    $model = $this->_objectManager->create(\Agrekom\ProductConfigurator\Model\Steps::class)->load($modelid);
                    try {

                        $code = $postItems[$modelid]['code'];
                        if (strpos($code, 'step_') === false) {
                            $messages[] = "[Steps ID: {$modelid}]  {'Wrong code pattern. Is should look like step_{number}.'}";
                            $error = true;
                            continue;
                        }

                        $codeArr = explode('_', $code);
                        $codeNumber = (int) $codeArr[1];
                        if (!is_int($codeNumber) OR $codeNumber <= 10) {
                            $messages[] = "[Steps ID: {$modelid}]  {'Missing code number or number is not bigger then 10.'}";
                            $error = true;
                            continue;
                        }

                        $model->setData(array_merge($model->getData(), $postItems[$modelid]));
                        $model->save();
                    } catch (\Exception $e) {
                        $messages[] = "[Steps ID: {$modelid}]  {$e->getMessage()}";
                        $error = true;
                    }
                }
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }
}


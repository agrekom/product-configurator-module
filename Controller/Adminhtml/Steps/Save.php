<?php

namespace Agrekom\ProductConfigurator\Controller\Adminhtml\Steps;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{

    protected $dataPersistor;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('steps_id');

            $model = $this->_objectManager->create(\Agrekom\ProductConfigurator\Model\Steps::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This step no longer exists.'));

                return $resultRedirect->setPath('*/*/');
            }

            $code = $data['code'];
            if (strpos($code, 'step_') === false) {
                $this->messageManager->addErrorMessage(__('Wrong code pattern. Is should look like step_{number}.'));

                return $resultRedirect->setPath('*/*/');
            }

            $codeArr = explode('_', $code);
            $codeNumber = (int) $codeArr[1];
            if (!is_int($codeNumber) OR $codeNumber <= 10) {
                $this->messageManager->addErrorMessage(__('Missing code number or number is not bigger then 10.'));

                return $resultRedirect->setPath('*/*/');
            }


            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved the step.'));
                $this->dataPersistor->clear('agrekom_productconfigurator_steps');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['steps_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the step.'));
            }

            $this->dataPersistor->set('agrekom_productconfigurator_steps', $data);
            return $resultRedirect->setPath('*/*/edit', ['steps_id' => $this->getRequest()->getParam('steps_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}


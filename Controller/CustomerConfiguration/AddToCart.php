<?php

namespace Agrekom\ProductConfigurator\Controller\CustomerConfiguration;

class AddToCart implements \Magento\Framework\App\Action\HttpGetActionInterface
{

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Agrekom\ProductConfigurator\Model\CustomerConfiguration\AddToCart
     */
    protected $addToCartCustomerConfiguration;

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $redirectFactory;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Agrekom\ProductConfigurator\Model\CustomerConfiguration\AddToCart $addToCartCustomerConfiguration,
        \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager
    )
    {
        $this->request = $request;
        $this->addToCartCustomerConfiguration = $addToCartCustomerConfiguration;
        $this->redirectFactory = $redirectFactory;
        $this->messageManager = $messageManager;
    }

    public function execute()
    {
        $configurationId = $this->request->getParam('configuration_id', '');
        $this->addToCartCustomerConfiguration->execute($configurationId);

        $this->messageManager->addSuccessMessage(__("%1 configuration products added to the cart", $configurationId));
        $resultRedirect = $this->redirectFactory->create();
        $resultRedirect->setPath('checkout/cart');

        return $resultRedirect;
    }

}

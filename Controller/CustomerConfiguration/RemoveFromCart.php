<?php

namespace Agrekom\ProductConfigurator\Controller\CustomerConfiguration;

class RemoveFromCart implements \Magento\Framework\App\Action\HttpGetActionInterface
{

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Agrekom\ProductConfigurator\Model\CustomerConfiguration\RemoveFromCart
     */
    protected $removeFromCartCustomerConfiguration;

    /**
     * @var \Magento\Framework\Controller\Result\RedirectFactory
     */
    protected $redirectFactory;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Agrekom\ProductConfigurator\Model\CustomerConfiguration\RemoveFromCart $removeFromCartCustomerConfiguration,
        \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager
    )
    {
        $this->request = $request;
        $this->removeFromCartCustomerConfiguration = $removeFromCartCustomerConfiguration;
        $this->redirectFactory = $redirectFactory;
        $this->messageManager = $messageManager;
    }

    public function execute()
    {
        $redirectToCart = $this->request->getParam('redirect_to_cart', false);
        $configurationId = $this->request->getParam('configuration_id', '');

        $this->removeFromCartCustomerConfiguration->execute($configurationId);

        $resultRedirect = $this->redirectFactory->create();
        if ($redirectToCart) {
            $this->messageManager->addWarningMessage(__("%1 configuration products removed from the cart", $configurationId));
            $resultRedirect->setPath('checkout/cart');
        } else {
            $resultRedirect->setPath('product-configurator/customerconfiguration/addtocart', [
                'configuration_id' => $configurationId
            ]);
        }

        return $resultRedirect;
    }

}

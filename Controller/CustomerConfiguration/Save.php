<?php

namespace Agrekom\ProductConfigurator\Controller\CustomerConfiguration;

use Agrekom\ProductConfigurator\Helper\SendCustomerConfigurationNotificationEmail;

class Save implements \Magento\Framework\App\Action\HttpGetActionInterface
{

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Agrekom\ProductConfigurator\Model\CustomerConfiguration\Save
     */
    protected $saveCustomerConfiguration;

    /**
     * @var \Agrekom\ProductConfigurator\Helper\SystemConfiguration
     */
    protected $systemConfiguration;

    /**
     * @var \Agrekom\ProductConfigurator\Helper\SendCustomerConfigurationNotificationEmail
     */
    protected $customerConfigurationNotificationEmail;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Agrekom\ProductConfigurator\Model\CustomerConfiguration\Save $saveCustomerConfiguration,
        \Agrekom\ProductConfigurator\Helper\SystemConfiguration $systemConfiguration,
        \Agrekom\ProductConfigurator\Helper\SendCustomerConfigurationNotificationEmail $customerConfigurationNotificationEmail,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->request = $request;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->saveCustomerConfiguration = $saveCustomerConfiguration;
        $this->systemConfiguration = $systemConfiguration;
        $this->customerConfigurationNotificationEmail = $customerConfigurationNotificationEmail;
        $this->logger = $logger;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $json = $this->request->getParam('notification', '{}');
        $response = [
            'error' => false,
            'message' => __("Configuration saved and sent successfully. Thank you.")
        ];

        try {
            $configurationId = $this->saveCustomerConfiguration->execute($json);
            $this->customerConfigurationNotificationEmail->execute($json, $configurationId);
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
            $response = [
                'error' => true,
                'message' => __("There was an error when saving you configuration. Please contact us directly to email %1", $this->systemConfiguration->getNotificationEmailToEmail())
            ];
        }

        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultJsonFactory->create();

        return $result->setData($response);
    }

}

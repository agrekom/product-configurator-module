<?php

namespace Agrekom\ProductConfigurator\Controller\NotificationEmail;

use Agrekom\ProductConfigurator\Helper\SendNotificationEmail;

class Send implements \Magento\Framework\App\Action\HttpGetActionInterface
{

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Agrekom\ProductConfigurator\Helper\SendNotificationEmail
     */
    protected $sendNotificationEmail;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Agrekom\ProductConfigurator\Helper\SendNotificationEmail $sendNotificationEmail,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    )
    {
        $this->request = $request;
        $this->sendNotificationEmail = $sendNotificationEmail;
        $this->resultJsonFactory = $resultJsonFactory;
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $jsonTestData = json_encode([
            'message' => 'marceli.podstawski@gmail.com',
            'products' => [
                ['id' => 27, 'qty' => 1],
                ['id' => 28, 'qty' => 1],
                ['id' => 30, 'qty' => 1],
            ]
        ]);

        # {"message":"marceli.podstawski@gmail.com","products":[{"id":27,"qty":1},{"id":28,"qty":1},{"id":30,"qty":1}]}

        /** @var \Magento\Framework\Controller\Result\Json $result */
        $result = $this->resultJsonFactory->create();

        $json = $this->request->getParam('notification', '{}');
        $response = $this->sendNotificationEmail->execute($json);

        return $result->setData($response);
    }

}

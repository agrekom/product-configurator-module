<?php

namespace Agrekom\ProductConfigurator\Helper;

class Constants
{

    const DEFAULT_VAT_PERCENT_TAX_RATE = '23';
    const DEFAULT_CURRENCY_CODE = 'PLN';

    const PRODUCT_CONFIGURATOR_NOTIFICATION_EMAIL_TEMPLATE = 'notification_email_configuration_email_template';

    const NOTIFICATION_EMAIL_CONFIGURATION_SENDER_EMAIL_PATH = 'notification_email/configuration/sender_email';
    const NOTIFICATION_EMAIL_CONFIGURATION_SENDER_NAME_PATH = 'notification_email/configuration/sender_name';
    const NOTIFICATION_EMAIL_CONFIGURATION_TO_EMAIL_PATH = 'notification_email/configuration/to_email';
    const NOTIFICATION_EMAIL_CONFIGURATION_TO_NAME_PATH = 'notification_email/configuration/to_name';
    const NOTIFICATION_EMAIL_CONFIGURATION_EMAIL_TEMPLATE_PATH = 'notification_email/configuration/email_template';

    const CUSTOMER_CONFIGURATION_NOTIFICATION_EMAIL_SENDER_PATH = 'product_configurator_customer_configuration/configuration/sender_email';
    const CUSTOMER_CONFIGURATION_NOTIFICATION_EMAIL_SENDER_NAME_PATH = 'product_configurator_customer_configuration/configuration/sender_name';
    const CUSTOMER_CONFIGURATION_NOTIFICATION_EMAIL_TEMPLATE_PATH = 'product_configurator_customer_configuration/configuration/email_template';

    const VAT_RATES_CONFIGURATION_VAT_PERCENT_PATH = 'vat_rates/configuration/vat_percent';

    const MODULE_NAME = 'product-configurator';
    const ADD_TO_CART_ACTION_NAME = 'addtocart';

}

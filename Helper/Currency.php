<?php

namespace Agrekom\ProductConfigurator\Helper;

class Currency extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    /**
     * @return string
     */
    public function renderCurrencyCode(): string
    {
        try {
            $currencyCode = $this->storeManager->getStore()->getCurrentCurrency()->getCode();
        } catch (\Exception $exception) {
            $currencyCode = \Agrekom\ProductConfigurator\Helper\Constants::DEFAULT_CURRENCY_CODE;
        }

        return $currencyCode;
    }

}

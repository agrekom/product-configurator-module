<?php

namespace Agrekom\ProductConfigurator\Helper;

class PrepareCustomerConfigurationCustomerEmail
{

    /**
     * @param string $json
     * @return string
     * @throws \Exception
     */
    public function execute(string $json)
    {
        $arr = json_decode($json, true);
        if (!array_key_exists('email', $arr)) {
            throw new \Exception(__('Missing customer email.'));
        }

        return $arr['email'];
    }

}

<?php

namespace Agrekom\ProductConfigurator\Helper;

class PrepareNotificationEmailCustomerMessage
{

    /**
     * @param string $json
     * @return string
     * @throws \Exception
     */
    public function execute(string $json)
    {
        $arr = json_decode($json, true);
        if (!array_key_exists('message', $arr)) {
            throw new \Exception(__('Missing customer message to send.'));
        }

        return $arr['message'];
    }

}

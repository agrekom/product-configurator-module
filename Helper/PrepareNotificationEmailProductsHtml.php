<?php

namespace Agrekom\ProductConfigurator\Helper;

class PrepareNotificationEmailProductsHtml
{

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    )
    {
        $this->storeManager = $storeManager;
        $this->productRepository = $productRepository;
    }

    /**
     * @param string $json
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(string $json)
    {
        $arr = json_decode($json, true);
        if (!array_key_exists('products', $arr)) {
            throw new \Exception(__('Missing products to send.'));
        }

        $products = $arr['products'];
        $storeId = $this->storeManager->getStore()->getId();
        $html = sprintf("<table cellpadding='5'><tr><th><b>%s</b></th><th><b>%s</b></th><th><b>%s</b></th><th><b>%s</b></th></tr>", __('ID'), __('Name'), __('SKU'), __('QTY'));
        foreach ($products as $item) {
            $productId = $item['id'];
            $product = $this->productRepository->getById($productId, false, $storeId);
            $url = $product->setStoreId($storeId)->getUrlModel()->getUrlInStore($product, ['_escape' => true]);
            $qty = $this->findQty($products, $productId);
            $html .= sprintf("<tr><td>%s</td><td><a href='%s'>%s</a></td><td>%s</td><td>%s</td></tr>", $productId, $url, $product->getName(), $product->getSku(), $qty);
        }
        $html .= '</table>';

        return $html;
    }

    /**
     * @param array $arr
     * @param int $productId
     * @return int
     */
    protected function findQty(array $arr, int $productId): int
    {
        foreach ($arr as $item) {
            if ($item['id'] != $productId) {
                continue;
            }

            return (int) $item['qty'];
        }

        return 1;
    }

}

<?php

namespace Agrekom\ProductConfigurator\Helper;

class PriceSummaryText extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Agrekom\ProductConfigurator\Helper\SystemConfiguration
     */
    protected $systemConfiguration;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Agrekom\ProductConfigurator\Helper\SystemConfiguration $systemConfiguration
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->systemConfiguration = $systemConfiguration;
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function render()
    {
        $vatPercentTaxRate = $this->systemConfiguration->getVatPercentTaxRate();

        return __("including %1% vat", $vatPercentTaxRate);
    }

}

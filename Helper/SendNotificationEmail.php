<?php

namespace Agrekom\ProductConfigurator\Helper;

class SendNotificationEmail extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Agrekom\ProductConfigurator\Helper\PrepareNotificationEmailProductsHtml
     */
    protected $prepareNotificationEmailProductsHtml;

    /**
     * @var \Agrekom\ProductConfigurator\Helper\PrepareNotificationEmailCustomerMessage
     */
    protected $prepareNotificationEmailCustomerMessage;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Magento\Framework\Escaper
     */
    protected $escaper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Agrekom\ProductConfigurator\Helper\SystemConfiguration
     */
    protected $systemConfiguration;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Agrekom\ProductConfigurator\Helper\PrepareNotificationEmailProductsHtml $prepareNotificationEmailProductsHtml,
        \Agrekom\ProductConfigurator\Helper\PrepareNotificationEmailCustomerMessage $prepareNotificationEmailCustomerMessage,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Agrekom\ProductConfigurator\Helper\SystemConfiguration $systemConfiguration
    )
    {
        $this->prepareNotificationEmailProductsHtml = $prepareNotificationEmailProductsHtml;
        $this->prepareNotificationEmailCustomerMessage = $prepareNotificationEmailCustomerMessage;
        $this->inlineTranslation = $inlineTranslation;
        $this->escaper = $escaper;
        $this->transportBuilder = $transportBuilder;
        $this->storeManager = $storeManager;
        $this->systemConfiguration = $systemConfiguration;

        parent::__construct($context);
    }

    /**
     * @param string $json
     * @return array
     */
    public function execute(string $json)
    {
        try {
            $notificationEmailProductsHtml = $this->prepareNotificationEmailProductsHtml->execute($json);
            $notificationEmailCustomerMessage = $this->prepareNotificationEmailCustomerMessage->execute($json);
            $this->inlineTranslation->suspend();
            $sender = [
                'name' => $this->systemConfiguration->getNotificationEmailSenderName(),
                'email' => $this->systemConfiguration->getNotificationEmailSenderEmail(),
            ];
            $transport = $this->transportBuilder
                ->setTemplateIdentifier($this->systemConfiguration->getNotificationEmailTemplate())
                ->setTemplateOptions(
                    [
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => $this->storeManager->getStore()->getId(),
                    ]
                )
                ->setTemplateVars([
                    'customerMessage'  => $notificationEmailCustomerMessage,
                    'products' => $notificationEmailProductsHtml
                ])
                ->setFrom($sender)
                ->addTo(
                    $this->systemConfiguration->getNotificationEmailToEmail(),
                    $this->systemConfiguration->getNotificationEmailToName()
                )
                ->getTransport();
            $transport->sendMessage();
            $this->inlineTranslation->resume();

            return [
                'error' => false,
                'message' => __('Email sent successfully. Thank You.')
            ];

        } catch (\Exception $exception) {
            $this->_logger->error($exception->getMessage());
        }

        return [
            'error' => true,
            'message' => __('Sorry but there was an error while sending email. Please contact us directly to email %1', $this->systemConfiguration->getNotificationEmailToEmail())
        ];
    }

}

<?php

namespace Agrekom\ProductConfigurator\Helper;

class SystemConfiguration
{

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return string
     */
    public function getNotificationEmailSenderEmail(): string
    {
        return $this->scopeConfig->getValue(
            \Agrekom\ProductConfigurator\Helper\Constants::NOTIFICATION_EMAIL_CONFIGURATION_SENDER_EMAIL_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getNotificationEmailSenderName(): string
    {
        return $this->scopeConfig->getValue(
            \Agrekom\ProductConfigurator\Helper\Constants::NOTIFICATION_EMAIL_CONFIGURATION_SENDER_NAME_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getNotificationEmailToEmail(): string
    {
        return $this->scopeConfig->getValue(
            \Agrekom\ProductConfigurator\Helper\Constants::NOTIFICATION_EMAIL_CONFIGURATION_TO_EMAIL_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getNotificationEmailToName(): string
    {
        return $this->scopeConfig->getValue(
            \Agrekom\ProductConfigurator\Helper\Constants::NOTIFICATION_EMAIL_CONFIGURATION_TO_NAME_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getNotificationEmailTemplate(): string
    {
        return $this->scopeConfig->getValue(
            \Agrekom\ProductConfigurator\Helper\Constants::NOTIFICATION_EMAIL_CONFIGURATION_EMAIL_TEMPLATE_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getVatPercentTaxRate(): string
    {
        return $this->scopeConfig->getValue(
            \Agrekom\ProductConfigurator\Helper\Constants::VAT_RATES_CONFIGURATION_VAT_PERCENT_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getCustomerConfigurationNotificationEmailTemplate(): string
    {
        return $this->scopeConfig->getValue(
            \Agrekom\ProductConfigurator\Helper\Constants::CUSTOMER_CONFIGURATION_NOTIFICATION_EMAIL_TEMPLATE_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getCustomerConfigurationNotificationSenderEmail(): string
    {
        return $this->scopeConfig->getValue(
            \Agrekom\ProductConfigurator\Helper\Constants::CUSTOMER_CONFIGURATION_NOTIFICATION_EMAIL_SENDER_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getCustomerConfigurationNotificationSenderName(): string
    {
        return $this->scopeConfig->getValue(
            \Agrekom\ProductConfigurator\Helper\Constants::CUSTOMER_CONFIGURATION_NOTIFICATION_EMAIL_SENDER_NAME_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

}

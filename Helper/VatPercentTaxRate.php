<?php

namespace Agrekom\ProductConfigurator\Helper;

class VatPercentTaxRate extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var \Agrekom\ProductConfigurator\Helper\SystemConfiguration
     */
    protected $systemConfiguration;

    public function __construct(\Agrekom\ProductConfigurator\Helper\SystemConfiguration $systemConfiguration)
    {
        $this->systemConfiguration = $systemConfiguration;
    }

    /**
     * @return string
     */
    public function render()
    {
        $vatPercentTaxRate = $this->systemConfiguration->getVatPercentTaxRate();
        if (!$vatPercentTaxRate) {
            $vatPercentTaxRate = \Agrekom\ProductConfigurator\Helper\Constants::DEFAULT_VAT_PERCENT_TAX_RATE;
        }

        /**
         * @codeCoverageIgnore $this->helper('Agrekom\ProductConfigurator\Helper\VatPercentTaxRate')->render()
         */

        return $vatPercentTaxRate;
    }

}

<?php

namespace Agrekom\ProductConfigurator\Model;

class AddProductsToCart
{

    protected $cart;

    public function __construct(\Magento\Checkout\Model\Cart $cart)
    {
        $this->cart = $cart;
    }

    public function execute(array $productIds)
    {
        $this->cart->addProductsByIds($productIds);
        $this->cart->save();
    }

}

<?php

namespace Agrekom\ProductConfigurator\Model\AddToCart;

class AddProductsToCart
{

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;

    /**
     * @var PrepareProductIds
     */
    protected $prepareProductIds;

    public function __construct(
        \Magento\Checkout\Model\Cart $cart,
        \Agrekom\ProductConfigurator\Model\AddToCart\PrepareProductIds $prepareProductIds
    )
    {
        $this->cart = $cart;
        $this->prepareProductIds = $prepareProductIds;
    }

    public function execute(string $json)
    {
        $productIds = $this->prepareProductIds->execute($json);
        $this->cart->addProductsByIds($productIds)->save();
    }

}

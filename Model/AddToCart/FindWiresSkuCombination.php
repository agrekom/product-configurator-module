<?php

namespace Agrekom\ProductConfigurator\Model\AddToCart;

class FindWiresSkuCombination
{

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var \Agrekom\ProductConfigurator\Api\WiresSkuCombinationRepositoryInterface
     */
    protected $wiresSkuCombinationRepository;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    public function __construct(
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Agrekom\ProductConfigurator\Api\WiresSkuCombinationRepositoryInterface $wiresSkuCombinationRepository,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->productRepository = $productRepository;
        $this->wiresSkuCombinationRepository = $wiresSkuCombinationRepository;
        $this->messageManager = $messageManager;
        $this->logger = $logger;
    }

    /**
     * @param array $productIds
     * @return array
     */
    public function execute(array $productIds)
    {
        try {
            $combinationToSearchArr = $this->prepareCombinationToSearch($productIds);
            $combinations = $this->getCombinations();
            foreach ($combinations as $combination) {
                $combinationArr = explode(',', $combination->getSkuCombination());
                sort($combinationArr);
                if ($combinationArr != $combinationToSearchArr) {
                    continue;
                }

                $this->messageManager->addNoticeMessage(__('product_configurator_sku_combination_add_to_cart_msg'));

                return $this->getFoundCombinationProductIds($combination->getSku());
            }
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
        }

        return $productIds;
    }

    /**
     * @param $skus
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getFoundCombinationProductIds($skus)
    {
        $productIds = [];
        $skus = explode(",", $skus);
        foreach ($skus as $sku) {
            $sku = trim($sku);
            $product = $this->productRepository->get($sku);
            $productIds[] = $product->getId();
        }

        return $productIds;
    }

    /**
     * @return \Agrekom\ProductConfigurator\Api\Data\WiresSkuCombinationInterface[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getCombinations()
    {
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $combinations = $this->wiresSkuCombinationRepository->getList($searchCriteria);

        return $combinations->getItems();
    }

    /**
     * @param array $productIds
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function prepareCombinationToSearch(array $productIds)
    {
        $skus = [];
        foreach ($productIds as $productId) {
            $product = $this->productRepository->getById($productId);
            $skus[] = $product->getSku();
        }

        sort($skus);

        return $skus;
    }

}

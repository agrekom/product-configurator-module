<?php

namespace Agrekom\ProductConfigurator\Model\AddToCart;

class PrepareProductIds
{

    /**
     * @var \Agrekom\ProductConfigurator\Model\AddToCart\FindWiresSkuCombination
     */
    protected $findWiresSkuCombination;

    public function __construct(
        \Agrekom\ProductConfigurator\Model\AddToCart\FindWiresSkuCombination $findWiresSkuCombination
    )
    {
        $this->findWiresSkuCombination = $findWiresSkuCombination;
    }

    /**
     * @param string $json
     * @return array
     */
    public function execute(string $json)
    {
        $productIds = [];
        $productWiresIds = [];

        $arr = json_decode($json, true);
        foreach ($arr as $item) {
            if ($item['isWire']) {
                $productWiresIds[] = $item['id'];
                continue;
            }
            $productIds[] = $item['id'];
        }

        if (count($productWiresIds)) {
            $wiresIds = $this->findWiresSkuCombination->execute($productWiresIds);
            $productIds = array_merge($productIds, $wiresIds);
        }

        return $productIds;
    }

}

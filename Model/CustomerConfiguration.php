<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Agrekom\ProductConfigurator\Model;

use Agrekom\ProductConfigurator\Api\Data\CustomerConfigurationInterface;
use Magento\Framework\Model\AbstractModel;

class CustomerConfiguration extends AbstractModel implements CustomerConfigurationInterface
{

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Agrekom\ProductConfigurator\Model\ResourceModel\CustomerConfiguration::class);
    }

    /**
     * @inheritDoc
     */
    public function getCustomerconfigurationId()
    {
        return $this->getData(self::CUSTOMERCONFIGURATION_ID);
    }

    /**
     * @inheritDoc
     */
    public function setCustomerconfigurationId($customerconfigurationId)
    {
        return $this->setData(self::CUSTOMERCONFIGURATION_ID, $customerconfigurationId);
    }

    /**
     * @inheritDoc
     */
    public function getProductId()
    {
        return $this->getData(self::PRODUCT_ID);
    }

    /**
     * @inheritDoc
     */
    public function setProductId($productId)
    {
        return $this->setData(self::PRODUCT_ID, $productId);
    }

    /**
     * @inheritDoc
     */
    public function getConfigurationId()
    {
        return $this->getData(self::CONFIGURATION_ID);
    }

    /**
     * @inheritDoc
     */
    public function setConfigurationId($configurationId)
    {
        return $this->setData(self::CONFIGURATION_ID, $configurationId);
    }

    /**
     * @inheritDoc
     */
    public function getCustomerId()
    {
        return $this->getData(self::CUSTOMER_ID);
    }

    /**
     * @inheritDoc
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }
}


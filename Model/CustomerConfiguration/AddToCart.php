<?php

namespace Agrekom\ProductConfigurator\Model\CustomerConfiguration;

class AddToCart
{

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Agrekom\ProductConfigurator\Api\CustomerConfigurationRepositoryInterface
     */
    protected $customerConfigurationRepository;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $product;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;

    public function __construct(
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Agrekom\ProductConfigurator\Api\CustomerConfigurationRepositoryInterface $customerConfigurationRepository,
        \Magento\Catalog\Model\Product $product,
        \Magento\Checkout\Model\Cart $cart
    )
    {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->customerConfigurationRepository = $customerConfigurationRepository;
        $this->product = $product;
        $this->cart = $cart;
    }

    /**
     * @param string $configurationId
     * @return void
     * @throws \Exception
     */
    public function execute(string $configurationId)
    {
        try {
            $searchCriteria = $this->searchCriteriaBuilder->addFilter(
                \Agrekom\ProductConfigurator\Api\Data\CustomerConfigurationInterface::CONFIGURATION_ID,
                $configurationId,
                'eq'
            )->create();

            $customerConfigurationList = $this->customerConfigurationRepository->getList($searchCriteria)->getItems();
            /** @var \Agrekom\ProductConfigurator\Api\Data\CustomerConfigurationInterface $customerConfiguration */
            foreach ($customerConfigurationList as $customerConfiguration) {
                $product = $this->product->loadByAttribute('entity_id', $customerConfiguration->getProductId());
                $quoteItem = $this->cart->getQuote()->addProduct($product, 1);
                $quoteItem->setData('customer_configuration_id', $customerConfiguration->getConfigurationId());
            }
            $this->cart->save();
        } catch (\Exception $exception) {
            throw new \Exception(__("Can not add all products from configuration to cart."));
        }
    }

}

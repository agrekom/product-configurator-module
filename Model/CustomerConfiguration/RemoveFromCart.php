<?php

namespace Agrekom\ProductConfigurator\Model\CustomerConfiguration;

class RemoveFromCart
{

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;

    public function __construct(\Magento\Checkout\Model\Cart $cart)
    {
        $this->cart = $cart;
    }

    /**
     * @param string $configurationId
     * @return void
     */
    public function execute(string $configurationId)
    {
        $saveCart = false;
        $quoteItems = $this->cart->getQuote()->getAllItems();
        /** @var \Magento\Quote\Model\Quote\Item $quoteItem */
        foreach ($quoteItems as $quoteItem) {
            if ($quoteItem->getData('customer_configuration_id') != $configurationId) {
                continue;
            }

            $saveCart = true;
            $this->cart->getQuote()->removeItem($quoteItem->getId());
        }

        if ($saveCart) {
            $this->cart->save();
        }
    }

}

<?php

namespace Agrekom\ProductConfigurator\Model\CustomerConfiguration;

class Save
{

    /**
     * @var \Agrekom\ProductConfigurator\Api\Data\CustomerConfigurationInterfaceFactory
     */
    protected $customerConfigurationFactory;

    /**
     * @var \Agrekom\ProductConfigurator\Api\CustomerConfigurationRepositoryInterface
     */
    protected $customerConfigurationRepository;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    public function __construct(
        \Agrekom\ProductConfigurator\Api\Data\CustomerConfigurationInterfaceFactory $customerConfigurationFactory,
        \Agrekom\ProductConfigurator\Api\CustomerConfigurationRepositoryInterface $customerConfigurationRepository,
        \Magento\Customer\Model\Session $customerSession
    )
    {
        $this->customerConfigurationFactory = $customerConfigurationFactory;
        $this->customerConfigurationRepository = $customerConfigurationRepository;
        $this->customerSession = $customerSession;
    }

    /**
     * @param string $json
     * @return string
     * @throws \Exception
     */
    public function execute(string $json)
    {
        if (!$json) {
            throw new \Exception(__("No items to save."));
        }

        $arr = json_decode($json, true);
        if (!array_key_exists('products', $arr)) {
            throw new \Exception(__("Missing products to save."));
        }

        $configurationId = strtoupper(uniqid('APC-'));
        $customerId = $this->customerSession->getCustomerId();
        $products = $arr['products'];
        foreach ($products as $product) {
            try {
                /** @var \Agrekom\ProductConfigurator\Api\Data\CustomerConfigurationInterface $customerConfiguration */
                $customerConfiguration = $this->customerConfigurationFactory->create();
                $customerConfiguration->setConfigurationId($configurationId);
                $customerConfiguration->setCustomerId($customerId);
                $customerConfiguration->setProductId($product['id']);

                $this->customerConfigurationRepository->save($customerConfiguration);
            } catch (\Exception $exception) {
                throw new \Exception(__("Could not save product %1 for %2 configuration", $product['id'], $configurationId));
            }
        }

        return $configurationId;
    }

}

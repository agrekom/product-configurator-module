<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Agrekom\ProductConfigurator\Model;

use Agrekom\ProductConfigurator\Api\CustomerConfigurationRepositoryInterface;
use Agrekom\ProductConfigurator\Api\Data\CustomerConfigurationInterface;
use Agrekom\ProductConfigurator\Api\Data\CustomerConfigurationInterfaceFactory;
use Agrekom\ProductConfigurator\Api\Data\CustomerConfigurationSearchResultsInterfaceFactory;
use Agrekom\ProductConfigurator\Model\ResourceModel\CustomerConfiguration as ResourceCustomerConfiguration;
use Agrekom\ProductConfigurator\Model\ResourceModel\CustomerConfiguration\CollectionFactory as CustomerConfigurationCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class CustomerConfigurationRepository implements CustomerConfigurationRepositoryInterface
{

    /**
     * @var ResourceCustomerConfiguration
     */
    protected $resource;

    /**
     * @var CustomerConfiguration
     */
    protected $searchResultsFactory;

    /**
     * @var CustomerConfigurationInterfaceFactory
     */
    protected $customerConfigurationFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var CustomerConfigurationCollectionFactory
     */
    protected $customerConfigurationCollectionFactory;


    /**
     * @param ResourceCustomerConfiguration $resource
     * @param CustomerConfigurationInterfaceFactory $customerConfigurationFactory
     * @param CustomerConfigurationCollectionFactory $customerConfigurationCollectionFactory
     * @param CustomerConfigurationSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceCustomerConfiguration $resource,
        CustomerConfigurationInterfaceFactory $customerConfigurationFactory,
        CustomerConfigurationCollectionFactory $customerConfigurationCollectionFactory,
        CustomerConfigurationSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->customerConfigurationFactory = $customerConfigurationFactory;
        $this->customerConfigurationCollectionFactory = $customerConfigurationCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(
        CustomerConfigurationInterface $customerConfiguration
    ) {
        try {
            $this->resource->save($customerConfiguration);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the customerConfiguration: %1',
                $exception->getMessage()
            ));
        }
        return $customerConfiguration;
    }

    /**
     * @inheritDoc
     */
    public function get($customerConfigurationId)
    {
        $customerConfiguration = $this->customerConfigurationFactory->create();
        $this->resource->load($customerConfiguration, $customerConfigurationId);
        if (!$customerConfiguration->getId()) {
            throw new NoSuchEntityException(__('CustomerConfiguration with id "%1" does not exist.', $customerConfigurationId));
        }
        return $customerConfiguration;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->customerConfigurationCollectionFactory->create();
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(
        CustomerConfigurationInterface $customerConfiguration
    ) {
        try {
            $customerConfigurationModel = $this->customerConfigurationFactory->create();
            $this->resource->load($customerConfigurationModel, $customerConfiguration->getCustomerconfigurationId());
            $this->resource->delete($customerConfigurationModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the CustomerConfiguration: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($customerConfigurationId)
    {
        return $this->delete($this->get($customerConfigurationId));
    }
}


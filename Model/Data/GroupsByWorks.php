<?php

namespace Agrekom\ProductConfigurator\Model;

class GroupsByWorks
    extends \Magento\Framework\Model\AbstractModel
    implements \Agrekom\ProductConfigurator\Api\Data\GroupsByWorksInterface
{

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Agrekom\ProductConfigurator\Model\ResourceModel\GroupsByWorks::class);
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->_get(self::ID);
    }

    /**
     * @inheritDoc
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @inheritDoc
     */
    public function getCode()
    {
        return $this->_get(self::CODE);
    }

    /**
     * @inheritDoc
     */
    public function setCode($code)
    {
        return $this->setData(self::CODE, $code);
    }

    /**
     * @inheritDoc
     */
    public function getTitle()
    {
        return $this->_get(self::TITLE);
    }

    /**
     * @inheritDoc
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * @inheritDoc
     */
    public function getGroupId()
    {
        return $this->_get(self::GROUP_ID);
    }

    /**
     * @inheritDoc
     */
    public function setGroupId($groupId)
    {
        return $this->setData(self::GROUP_ID, $groupId);
    }
}


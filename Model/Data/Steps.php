<?php

namespace Agrekom\ProductConfigurator\Model\Data;

use Agrekom\ProductConfigurator\Api\Data\StepsInterface;

class Steps extends \Magento\Framework\Api\AbstractExtensibleObject implements StepsInterface
{

    /**
     * Get comparison_table_title
     * @return string|null
     */
    public function getComparisonTableTitle()
    {
        return $this->_get(self::COMPARISION_TABLE_TITLE);
    }

    /**
     * Set comparison_table_title
     * @param string $comparisonTableTitle
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsInterface
     */
    public function setComparisonTableTitle($comparisonTableTitle)
    {
        return $this->setData(self::COMPARISION_TABLE_TITLE, $comparisonTableTitle);
    }

    /**
     * Get purpose_product_id
     * @return int|null
     */
    public function getPurposeProductId()
    {
        return $this->_get(self::PURPOSE_PRODUCT_ID);
    }

    /**
     * Set purpose_product_id
     * @param int $purposeProductId
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsInterface
     */
    public function setPurposeProductId($purposeProductId)
    {
        return $this->setData(self::PURPOSE_PRODUCT_ID, $purposeProductId);
    }

    /**
     * Get page_layout
     * @return string|null
     */
    public function getPageLayout()
    {
        return $this->_get(self::PAGE_LAYOUT);
    }

    /**
     * Set page_layout
     * @param string $pageLayout
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsInterface
     */
    public function setPageLayout($pageLayout)
    {
        return $this->setData(self::PAGE_LAYOUT, $pageLayout);
    }

    /**
     * Get steps_id
     * @return string|null
     */
    public function getStepsId()
    {
        return $this->_get(self::STEPS_ID);
    }

    /**
     * Set steps_id
     * @param string $stepsId
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsInterface
     */
    public function setStepsId($stepsId)
    {
        return $this->setData(self::STEPS_ID, $stepsId);
    }

    /**
     * Get title
     * @return string|null
     */
    public function getTitle()
    {
        return $this->_get(self::TITLE);
    }

    /**
     * Set title
     * @param string $title
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Agrekom\ProductConfigurator\Api\Data\StepsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Agrekom\ProductConfigurator\Api\Data\StepsExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get code
     * @return string|null
     */
    public function getCode()
    {
        return $this->_get(self::CODE);
    }

    /**
     * Set code
     * @param string $code
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsInterface
     */
    public function setCode($code)
    {
        return $this->setData(self::CODE, $code);
    }

    /**
     * Get created_at
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->_get(self::CREATED_AT);
    }

    /**
     * Set created_at
     * @param string $createdAt
     * @return \Agrekom\ProductConfigurator\Api\Data\StepsInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}


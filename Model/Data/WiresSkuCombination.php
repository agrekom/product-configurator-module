<?php

namespace Agrekom\ProductConfigurator\Model\Data;

class WiresSkuCombination
    extends \Magento\Framework\Model\AbstractModel
    implements \Agrekom\ProductConfigurator\Api\Data\WiresSkuCombinationInterface
{

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Agrekom\ProductConfigurator\Model\ResourceModel\WiresSkuCombination::class);
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->_get(self::ID);
    }

    /**
     * @inheritDoc
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @inheritDoc
     */
    public function getSku()
    {
        return $this->_get(self::SKU);
    }

    /**
     * @inheritDoc
     */
    public function setSku($sku)
    {
        return $this->setData(self::SKU, $sku);
    }

    /**
     * @inheritDoc
     */
    public function getSkuCombination()
    {
        return $this->_get(self::SKU_COMBINATION);
    }

    /**
     * @inheritDoc
     */
    public function setSkuCombination($skuCombination)
    {
        return $this->setData(self::SKU_COMBINATION, $skuCombination);
    }

}


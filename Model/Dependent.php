<?php

namespace Agrekom\ProductConfigurator\Model;

class Dependent extends \Magento\Framework\DataObject
{

    /**
     * @var \Magento\Catalog\Model\Product\Link
     */
    protected $linkInstance;

    /**
     * @param \Agrekom\ProductConfigurator\Model\Product\Link $productLink
     */
    public function __construct(\Agrekom\ProductConfigurator\Model\Product\Link $productLink)
    {
        $this->linkInstance = $productLink;
    }

    /**
     * @return  \Magento\Catalog\Model\Product\Link
     */
    public function getLinkInstance()
    {
        return $this->linkInstance;
    }

    /**
     * @param \Magento\Catalog\Model\Product $currentProduct
     * @return array
     */
    public function getDependentProducts(\Magento\Catalog\Model\Product $currentProduct)
    {
        if (!$this->hasDependentProducts()) {
            $products = [];
            $collection = $this->getDependentProductCollection($currentProduct);
            foreach ($collection as $product) {
                $products[] = $product;
            }
            $this->setDependentProducts($products);
        }

        return $this->getData('dependent_products');
    }

    /**
     * @param \Magento\Catalog\Model\Product $currentProduct
     * @return array
     */
    public function getDependentProductIds(\Magento\Catalog\Model\Product $currentProduct)
    {
        if (!$this->hasDependentProductIds()) {
            $ids = [];
            foreach ($this->getDependentProducts($currentProduct) as $product) {
                $ids[] = $product->getId();
            }
            $this->setDependentProductIds($ids);
        }

        return $this->getData('dependent_product_ids');
    }

    /**
     * @param \Magento\Catalog\Model\Product $currentProduct
     * @return \Magento\Catalog\Model\ResourceModel\Product\Link\Product\Collection
     */
    public function getDependentProductCollection(\Magento\Catalog\Model\Product $currentProduct)
    {
        $collection = $this->getLinkInstance()->useDependentLinks()->getProductCollection()->setIsStrongMode();
        $collection->setProduct($currentProduct);

        return $collection;
    }

    /**
     * @param \Magento\Catalog\Model\Product $currentProduct
     * @return \Magento\Catalog\Model\ResourceModel\Product\Link\Collection
     */
    public function getDependentLinkCollection(\Magento\Catalog\Model\Product $currentProduct)
    {
        $collection = $this->getLinkInstance()->useDependentLinks()->getLinkCollection();
        $collection->setProduct($currentProduct);
        $collection->addLinkTypeIdFilter();
        $collection->addProductIdFilter();
        $collection->joinAttributes();

        return $collection;
    }

}

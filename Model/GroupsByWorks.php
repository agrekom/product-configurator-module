<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Agrekom\ProductConfigurator\Model;

use Agrekom\ProductConfigurator\Api\Data\GroupsByWorksInterface;
use Agrekom\ProductConfigurator\Api\Data\GroupsByWorksInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class GroupsByWorks extends \Magento\Framework\Model\AbstractModel
{

    protected $_eventPrefix = 'agrekom_productconfigurator_groupsbyworks';
    protected $groupsbyworksDataFactory;

    protected $dataObjectHelper;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param GroupsByWorksInterfaceFactory $groupsbyworksDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Agrekom\ProductConfigurator\Model\ResourceModel\GroupsByWorks $resource
     * @param \Agrekom\ProductConfigurator\Model\ResourceModel\GroupsByWorks\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        GroupsByWorksInterfaceFactory $groupsbyworksDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Agrekom\ProductConfigurator\Model\ResourceModel\GroupsByWorks $resource,
        \Agrekom\ProductConfigurator\Model\ResourceModel\GroupsByWorks\Collection $resourceCollection,
        array $data = []
    ) {
        $this->groupsbyworksDataFactory = $groupsbyworksDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve groupsbyworks model with groupsbyworks data
     * @return GroupsByWorksInterface
     */
    public function getDataModel()
    {
        $groupsbyworksData = $this->getData();

        $groupsbyworksDataObject = $this->groupsbyworksDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $groupsbyworksDataObject,
            $groupsbyworksData,
            GroupsByWorksInterface::class
        );

        return $groupsbyworksDataObject;
    }
}

<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Agrekom\ProductConfigurator\Model;

use Agrekom\ProductConfigurator\Api\Data\GroupsByWorksInterface;
use Agrekom\ProductConfigurator\Api\Data\GroupsByWorksInterfaceFactory;
use Agrekom\ProductConfigurator\Api\Data\GroupsByWorksSearchResultsInterfaceFactory;
use Agrekom\ProductConfigurator\Api\GroupsByWorksRepositoryInterface;
use Agrekom\ProductConfigurator\Model\ResourceModel\GroupsByWorks as ResourceGroupsByWorks;
use Agrekom\ProductConfigurator\Model\ResourceModel\GroupsByWorks\CollectionFactory as GroupsByWorksCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class GroupsByWorksRepository implements GroupsByWorksRepositoryInterface
{

    /**
     * @var GroupsByWorks
     */
    protected $searchResultsFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var ResourceGroupsByWorks
     */
    protected $resource;

    /**
     * @var GroupsByWorksInterfaceFactory
     */
    protected $groupsByWorksFactory;

    /**
     * @var GroupsByWorksCollectionFactory
     */
    protected $groupsByWorksCollectionFactory;


    /**
     * @param ResourceGroupsByWorks $resource
     * @param GroupsByWorksInterfaceFactory $groupsByWorksFactory
     * @param GroupsByWorksCollectionFactory $groupsByWorksCollectionFactory
     * @param GroupsByWorksSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceGroupsByWorks $resource,
        GroupsByWorksInterfaceFactory $groupsByWorksFactory,
        GroupsByWorksCollectionFactory $groupsByWorksCollectionFactory,
        GroupsByWorksSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->groupsByWorksFactory = $groupsByWorksFactory;
        $this->groupsByWorksCollectionFactory = $groupsByWorksCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(GroupsByWorksInterface $groupsByWorks)
    {
        try {
            $this->resource->save($groupsByWorks);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the groupsByWorks: %1',
                $exception->getMessage()
            ));
        }
        return $groupsByWorks;
    }

    /**
     * @inheritDoc
     */
    public function get($groupsByWorksId)
    {
        $groupsByWorks = $this->groupsByWorksFactory->create();
        $this->resource->load($groupsByWorks, $groupsByWorksId);
        if (!$groupsByWorks->getId()) {
            throw new NoSuchEntityException(__('GroupsByWorks with id "%1" does not exist.', $groupsByWorksId));
        }
        return $groupsByWorks;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->groupsByWorksCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(GroupsByWorksInterface $groupsByWorks)
    {
        try {
            $groupsByWorksModel = $this->groupsByWorksFactory->create();
            $this->resource->load($groupsByWorksModel, $groupsByWorks->getId());
            $this->resource->delete($groupsByWorksModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the GroupsByWorks: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($groupsByWorksId)
    {
        return $this->delete($this->get($groupsByWorksId));
    }
}


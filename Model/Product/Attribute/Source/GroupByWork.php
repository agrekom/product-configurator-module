<?php

namespace Agrekom\ProductConfigurator\Model\Product\Attribute\Source;

class GroupByWork extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * @var \Agrekom\ProductConfigurator\Api\GroupsByWorksRepositoryInterface
     */
    protected $repository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @param \Agrekom\ProductConfigurator\Api\GroupsByWorksRepositoryInterface $repository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        \Agrekom\ProductConfigurator\Api\GroupsByWorksRepositoryInterface $repository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    )
    {
        $this->repository = $repository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $searchCriteria = $this->searchCriteriaBuilder->create();
        /** @var \Agrekom\ProductConfigurator\Api\Data\GroupsByWorksInterface $item */
        foreach ($this->repository->getList($searchCriteria)->getItems() as $item) {
            $this->_options[] = [
                'value' => $item->getCode(),
                'label' => $item->getTitle()
            ];
        }

        return $this->_options;
    }
}


<?php

namespace Agrekom\ProductConfigurator\Model\Product\Attribute\Source;

class ProductConfiguratorStep extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * @var \Agrekom\ProductConfigurator\Api\StepsRepositoryInterface
     */
    protected $stepsRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @param \Agrekom\ProductConfigurator\Api\StepsRepositoryInterface $stepsRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        \Agrekom\ProductConfigurator\Api\StepsRepositoryInterface $stepsRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    )
    {
        $this->stepsRepository = $stepsRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $itemsList = $this->stepsRepository->getList($searchCriteria);
        if (!$itemsList->getTotalCount()) {
            return [[]];
        }
        /** @var \Agrekom\ProductConfigurator\Api\Data\StepsInterface $item */
        foreach ($itemsList->getItems() as $item) {
            $this->_options[] = [
                'value' => $item->getCode(),
                'label' => $item->getTitle()
            ];
        }

        return $this->_options;
    }
}


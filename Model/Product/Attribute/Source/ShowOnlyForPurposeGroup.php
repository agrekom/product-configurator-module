<?php

namespace Agrekom\ProductConfigurator\Model\Product\Attribute\Source;

class ShowOnlyForPurposeGroup extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $this->_options[0] = [
            'value' => 0,
            'label' => __('All groups')
        ];

        $this->_options[\Agrekom\ProductConfigurator\Model\Source\GroupsByWorksGroup::GROUP_1_VALUE] = [
            'value' => \Agrekom\ProductConfigurator\Model\Source\GroupsByWorksGroup::GROUP_1_VALUE,
            'label' => __('Group 1')
        ];

        $this->_options[\Agrekom\ProductConfigurator\Model\Source\GroupsByWorksGroup::GROUP_2_VALUE] = [
            'value' => \Agrekom\ProductConfigurator\Model\Source\GroupsByWorksGroup::GROUP_2_VALUE,
            'label' => __('Group 2')
        ];

        $this->_options[\Agrekom\ProductConfigurator\Model\Source\GroupsByWorksGroup::GROUP_3_VALUE] = [
            'value' => \Agrekom\ProductConfigurator\Model\Source\GroupsByWorksGroup::GROUP_3_VALUE,
            'label' => __('Group 3')
        ];

        $this->_options[\Agrekom\ProductConfigurator\Model\Source\GroupsByWorksGroup::GROUP_4_VALUE] = [
            'value' => \Agrekom\ProductConfigurator\Model\Source\GroupsByWorksGroup::GROUP_4_VALUE,
            'label' => __('Group 4')
        ];

        $this->_options[\Agrekom\ProductConfigurator\Model\Source\GroupsByWorksGroup::GROUP_5_VALUE] = [
            'value' => \Agrekom\ProductConfigurator\Model\Source\GroupsByWorksGroup::GROUP_5_VALUE,
            'label' => __('Group 5')
        ];

        $this->_options[\Agrekom\ProductConfigurator\Model\Source\GroupsByWorksGroup::GROUP_6_VALUE] = [
            'value' => \Agrekom\ProductConfigurator\Model\Source\GroupsByWorksGroup::GROUP_6_VALUE,
            'label' => __('Group 6')
        ];

        $this->_options[\Agrekom\ProductConfigurator\Model\Source\GroupsByWorksGroup::GROUP_7_VALUE] = [
            'value' => \Agrekom\ProductConfigurator\Model\Source\GroupsByWorksGroup::GROUP_7_VALUE,
            'label' => __('Group 7')
        ];

        $this->_options[\Agrekom\ProductConfigurator\Model\Source\GroupsByWorksGroup::GROUP_8_VALUE] = [
            'value' => \Agrekom\ProductConfigurator\Model\Source\GroupsByWorksGroup::GROUP_8_VALUE,
            'label' => __('Group 8')
        ];

        $this->_options[\Agrekom\ProductConfigurator\Model\Source\GroupsByWorksGroup::GROUP_9_VALUE] = [
            'value' => \Agrekom\ProductConfigurator\Model\Source\GroupsByWorksGroup::GROUP_9_VALUE,
            'label' => __('Group 9')
        ];

        $this->_options[\Agrekom\ProductConfigurator\Model\Source\GroupsByWorksGroup::GROUP_10_VALUE] = [
            'value' => \Agrekom\ProductConfigurator\Model\Source\GroupsByWorksGroup::GROUP_10_VALUE,
            'label' => __('Group 10')
        ];

        return $this->_options;
    }
}


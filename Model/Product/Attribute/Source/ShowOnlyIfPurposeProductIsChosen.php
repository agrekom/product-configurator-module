<?php

namespace Agrekom\ProductConfigurator\Model\Product\Attribute\Source;

class ShowOnlyIfPurposeProductIsChosen extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    protected $purposeProducts;

    public function __construct(\Agrekom\ProductConfigurator\Model\Source\PurposeProductId $purposeProducts)
    {
        $this->purposeProducts = $purposeProducts;
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $options = $this->purposeProducts->toOptionArray();
        foreach ($options as $item) {
            $this->_options[] = [
                'value' => $item['value'],
                'label' => $item['label']
            ];
        }

        return $this->_options;
    }
}


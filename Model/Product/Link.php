<?php

namespace Agrekom\ProductConfigurator\Model\Product;

class Link extends \Magento\Catalog\Model\Product\Link
{

    const LINK_TYPE_DEPENDENT = 7;
    const LINK_TYPE_ACCESSORY = 8;

    /**
     * @return $this
     */
    public function useDependentLinks()
    {
        $this->setLinkTypeId(self::LINK_TYPE_DEPENDENT);

        return $this;
    }

    /**
     * @return $this
     */
    public function useAccessoryLinks()
    {
        $this->setLinkTypeId(self::LINK_TYPE_ACCESSORY);

        return $this;
    }

}

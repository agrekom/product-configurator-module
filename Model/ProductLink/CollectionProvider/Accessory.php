<?php

namespace Agrekom\ProductConfigurator\Model\ProductLink\CollectionProvider;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductLink\CollectionProviderInterface;

class Accessory implements CollectionProviderInterface
{
    /** @var \Agrekom\ProductConfigurator\Model\Accessory */
    protected $accessoryModel;

    /**
     * Accessory constructor.
     * @param \Agrekom\ProductConfigurator\Model\Accessory $accessoryModel
     */
    public function __construct(
        \Agrekom\ProductConfigurator\Model\Accessory $accessoryModel
    ) {
        $this->accessoryModel = $accessoryModel;
    }

    /**
     * {@inheritdoc}
     */
    public function getLinkedProducts(Product $product)
    {
        return (array) $this->accessoryModel->getAccessoryProducts($product);
    }
}

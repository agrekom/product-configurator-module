<?php

namespace Agrekom\ProductConfigurator\Model\ProductLink\CollectionProvider;

class Dependent implements \Magento\Catalog\Model\ProductLink\CollectionProviderInterface
{

    /**
     * @var \Agrekom\ProductConfigurator\Model\Dependent
     */
    protected $dependentModel;

    /**
     * @param \Agrekom\ProductConfigurator\Model\Dependent $dependentModel
     */
    public function __construct(\Agrekom\ProductConfigurator\Model\Dependent $dependentModel)
    {
        $this->dependentModel = $dependentModel;
    }

    /**
     * {@inheritdoc}
     */
    public function getLinkedProducts(\Magento\Catalog\Model\Product $product)
    {
        return (array) $this->dependentModel->getDependentProducts($product);
    }

}

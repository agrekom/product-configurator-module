<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Agrekom\ProductConfigurator\Model\ResourceModel\CustomerConfiguration;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @inheritDoc
     */
    protected $_idFieldName = 'customerconfiguration_id';

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(
            \Agrekom\ProductConfigurator\Model\CustomerConfiguration::class,
            \Agrekom\ProductConfigurator\Model\ResourceModel\CustomerConfiguration::class
        );
    }
}


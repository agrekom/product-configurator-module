<?php

namespace Agrekom\ProductConfigurator\Model\ResourceModel;

class Steps extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('agrekom_productconfigurator_steps', 'steps_id');
    }
}


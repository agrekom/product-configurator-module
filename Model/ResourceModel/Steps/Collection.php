<?php

namespace Agrekom\ProductConfigurator\Model\ResourceModel\Steps;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'steps_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Agrekom\ProductConfigurator\Model\Steps::class,
            \Agrekom\ProductConfigurator\Model\ResourceModel\Steps::class
        );
    }
}


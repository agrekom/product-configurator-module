<?php

namespace Agrekom\ProductConfigurator\Model\ResourceModel;

class WiresSkuCombination extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init('agrekom_productconfigurator_wiresskucombination', 'id');
    }

}


<?php

namespace Agrekom\ProductConfigurator\Model\Source;

class ComparisonTableTitle implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * @var array
     */
    protected $options;

    /**
     * @var string[]
     */
    protected $titleCodes = [
        'comparison_table' => 'Comparision table',
        'technical_specifications' => 'Technical Specifications'
    ];

    /**
     * @return string[]
     */
    public function toOptionArray()
    {
        $options = [];
        foreach ($this->titleCodes as $key => $val) {
            $options[] = [
                'value' => $key,
                'label' => __($val)
            ];
        }

        $this->options = $options;

        return $this->options;
    }

}

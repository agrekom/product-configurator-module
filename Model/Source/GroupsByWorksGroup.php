<?php

namespace Agrekom\ProductConfigurator\Model\Source;

class GroupsByWorksGroup implements \Magento\Framework\Option\ArrayInterface
{

    const GROUP_1_VALUE = 1;
    const GROUP_2_VALUE = 2;
    const GROUP_3_VALUE = 3;
    const GROUP_4_VALUE = 4;
    const GROUP_5_VALUE = 5;
    const GROUP_6_VALUE = 6;
    const GROUP_7_VALUE = 7;
    const GROUP_8_VALUE = 8;
    const GROUP_9_VALUE = 9;
    const GROUP_10_VALUE = 10;

    /**
     * @var array
     */
    protected $options;

    /**
     * @var string[]
     */
    protected $groupOptions = [
        self::GROUP_1_VALUE => 'Group 1',
        self::GROUP_2_VALUE => 'Group 2',
        self::GROUP_3_VALUE => 'Group 3',
        self::GROUP_4_VALUE => 'Group 4',
        self::GROUP_5_VALUE => 'Group 5',
        self::GROUP_6_VALUE => 'Group 6',
        self::GROUP_7_VALUE => 'Group 7',
        self::GROUP_8_VALUE => 'Group 8',
        self::GROUP_9_VALUE => 'Group 9',
        self::GROUP_10_VALUE => 'Group 10',
    ];

    /**
     * @return string[]
     */
    public function toOptionArray()
    {
        $options = [
            [
                'value' => '',
                'label' => ''
            ]
        ];

        foreach ($this->groupOptions as $key => $val) {
            $options[] = [
                'value' => $key,
                'label' => __($val)
            ];
        }

        $this->options = $options;

        return $this->options;
    }

}

<?php

namespace Agrekom\ProductConfigurator\Model\Source;

class PageLayout implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * @var array
     */
    protected $options;

    /**
     * @var string[]
     */
    protected $pageLayoutOptions = [
        //'purpose' => 'Przeznaczenie',
        'single_choice_not_obligatory_with_more_extensive_config' => 'Nieobowiazkowy wybór jednokrotny z bardziej rozbudowanym konfigiem',
        'single_choice_not_obligatory' => 'Nieobowiazkowy wybór jednokrotny',
        'single_choice_with_config' => 'Wybór jednokrotny z konfigiem',
        'single_choice_with_a_more_extensive_config' => 'Wybór jednokrotny z bardziej rozbudowanym konfigiem',
        'single_choice' => 'Wybór jednokrotny',
        //'multiple_selection_with_config' => 'Wybór wielokrotny z konfigiem',
        //'summary' => 'Podsumowanie'
    ];

    /**
     * @return string[]
     */
    public function toOptionArray()
    {
        $options = [
            [
                'value' => '',
                'label' => ''
            ]
        ];

        foreach ($this->pageLayoutOptions as $key => $val) {
            $options[] = [
                'value' => $key,
                'label' => $val
            ];
        }

        $this->options = $options;

        return $this->options;
    }

}

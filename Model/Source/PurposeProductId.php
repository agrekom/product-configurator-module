<?php

namespace Agrekom\ProductConfigurator\Model\Source;

class PurposeProductId implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * @var array
     */
    protected $options;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $productRepository;

    public function __construct(
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    )
    {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->productRepository = $productRepository;
    }

    /**
     * @return string[]
     */
    public function toOptionArray()
    {
        $products = $this->getProducts();
        $options[] = [
            'value' => 0,
            'label' => 'none'
        ];
        foreach ($products as $product) {
            $options[] = [
                'value' => $product->getId(),
                'label' => $product->getName()
            ];
        }

        $this->options = $options;

        return $this->options;
    }

    /**
     * @return \Magento\Catalog\Api\Data\ProductInterface[]
     */
    protected function getProducts()
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter(
            'is_purpose_product',
            1,
            'eq'
        )->create();
        $products = $this->productRepository->getList($searchCriteria);

        return $products->getItems();
    }

}

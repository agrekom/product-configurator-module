<?php

namespace Agrekom\ProductConfigurator\Model;

use Agrekom\ProductConfigurator\Api\Data\StepsInterface;
use Agrekom\ProductConfigurator\Api\Data\StepsInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Steps extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $_eventPrefix = 'agrekom_productconfigurator_steps';
    protected $stepsDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param StepsInterfaceFactory $stepsDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Agrekom\ProductConfigurator\Model\ResourceModel\Steps $resource
     * @param \Agrekom\ProductConfigurator\Model\ResourceModel\Steps\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        StepsInterfaceFactory $stepsDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Agrekom\ProductConfigurator\Model\ResourceModel\Steps $resource,
        \Agrekom\ProductConfigurator\Model\ResourceModel\Steps\Collection $resourceCollection,
        array $data = []
    ) {
        $this->stepsDataFactory = $stepsDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve steps model with steps data
     * @return StepsInterface
     */
    public function getDataModel()
    {
        $stepsData = $this->getData();
        
        $stepsDataObject = $this->stepsDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $stepsDataObject,
            $stepsData,
            StepsInterface::class
        );
        
        return $stepsDataObject;
    }
}


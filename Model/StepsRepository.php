<?php

namespace Agrekom\ProductConfigurator\Model;

use Agrekom\ProductConfigurator\Api\Data\StepsInterfaceFactory;
use Agrekom\ProductConfigurator\Api\Data\StepsSearchResultsInterfaceFactory;
use Agrekom\ProductConfigurator\Api\StepsRepositoryInterface;
use Agrekom\ProductConfigurator\Model\ResourceModel\Steps as ResourceSteps;
use Agrekom\ProductConfigurator\Model\ResourceModel\Steps\CollectionFactory as StepsCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class StepsRepository implements StepsRepositoryInterface
{

    protected $dataStepsFactory;

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $searchResultsFactory;

    protected $stepsFactory;

    private $storeManager;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;

    protected $stepsCollectionFactory;


    /**
     * @param ResourceSteps $resource
     * @param StepsFactory $stepsFactory
     * @param StepsInterfaceFactory $dataStepsFactory
     * @param StepsCollectionFactory $stepsCollectionFactory
     * @param StepsSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceSteps $resource,
        StepsFactory $stepsFactory,
        StepsInterfaceFactory $dataStepsFactory,
        StepsCollectionFactory $stepsCollectionFactory,
        StepsSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->stepsFactory = $stepsFactory;
        $this->stepsCollectionFactory = $stepsCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataStepsFactory = $dataStepsFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Agrekom\ProductConfigurator\Api\Data\StepsInterface $steps
    ) {
        /* if (empty($steps->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $steps->setStoreId($storeId);
        } */
        
        $stepsData = $this->extensibleDataObjectConverter->toNestedArray(
            $steps,
            [],
            \Agrekom\ProductConfigurator\Api\Data\StepsInterface::class
        );
        
        $stepsModel = $this->stepsFactory->create()->setData($stepsData);
        
        try {
            $this->resource->save($stepsModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the steps: %1',
                $exception->getMessage()
            ));
        }
        return $stepsModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($stepsId)
    {
        $steps = $this->stepsFactory->create();
        $this->resource->load($steps, $stepsId);
        if (!$steps->getId()) {
            throw new NoSuchEntityException(__('Steps with id "%1" does not exist.', $stepsId));
        }
        return $steps->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->stepsCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Agrekom\ProductConfigurator\Api\Data\StepsInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Agrekom\ProductConfigurator\Api\Data\StepsInterface $steps
    ) {
        try {
            $stepsModel = $this->stepsFactory->create();
            $this->resource->load($stepsModel, $steps->getStepsId());
            $this->resource->delete($stepsModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Steps: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($stepsId)
    {
        return $this->delete($this->get($stepsId));
    }
}


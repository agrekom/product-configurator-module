<?php

namespace Agrekom\ProductConfigurator\Model;

class WiresSkuCombination extends \Magento\Framework\Model\AbstractModel
{

    protected $_eventPrefix = 'agrekom_productconfigurator_wiresskucombination';

    /**
     * @var \Agrekom\ProductConfigurator\Api\Data\WiresSkuCombinationInterfaceFactory
     */
    protected $wiresskucombinationDataFactory;

    /**
     * @var \Magento\Framework\Api\DataObjectHelper
     */
    protected $dataObjectHelper;

    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Agrekom\ProductConfigurator\Api\Data\WiresSkuCombinationInterfaceFactory $wiresskucombinationDataFactory,
        \Magento\Framework\Api\DataObjectHelper $dataObjectHelper,
        \Agrekom\ProductConfigurator\Model\ResourceModel\WiresSkuCombination $resource,
        \Agrekom\ProductConfigurator\Model\ResourceModel\WiresSkuCombination\Collection $resourceCollection,
        array $data = []
    ) {
        $this->wiresskucombinationDataFactory = $wiresskucombinationDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @return \Agrekom\ProductConfigurator\Api\Data\WiresSkuCombinationInterface
     */
    public function getDataModel()
    {
        $data = $this->getData();

        $dataObject = $this->wiresskucombinationDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $dataObject,
            $data,
            \Agrekom\ProductConfigurator\Api\Data\WiresSkuCombinationInterface::class
        );

        return $dataObject;
    }

}

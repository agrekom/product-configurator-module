<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Agrekom\ProductConfigurator\Model;

use Agrekom\ProductConfigurator\Api\Data\WiresSkuCombinationInterface;
use Agrekom\ProductConfigurator\Api\Data\WiresSkuCombinationInterfaceFactory;
use Agrekom\ProductConfigurator\Api\Data\WiresSkuCombinationSearchResultsInterfaceFactory;
use Agrekom\ProductConfigurator\Api\WiresSkuCombinationRepositoryInterface;
use Agrekom\ProductConfigurator\Model\ResourceModel\WiresSkuCombination as ResourceWiresSkuCombination;
use Agrekom\ProductConfigurator\Model\ResourceModel\WiresSkuCombination\CollectionFactory as WiresSkuCombinationCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class WiresSkuCombinationRepository implements WiresSkuCombinationRepositoryInterface
{

    /**
     * @var WiresSkuCombination
     */
    protected $searchResultsFactory;

    /**
     * @var ResourceWiresSkuCombination
     */
    protected $resource;

    /**
     * @var WiresSkuCombinationInterfaceFactory
     */
    protected $wiresSkuCombinationFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var WiresSkuCombinationCollectionFactory
     */
    protected $wiresSkuCombinationCollectionFactory;


    /**
     * @param ResourceWiresSkuCombination $resource
     * @param WiresSkuCombinationInterfaceFactory $wiresSkuCombinationFactory
     * @param WiresSkuCombinationCollectionFactory $wiresSkuCombinationCollectionFactory
     * @param WiresSkuCombinationSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceWiresSkuCombination $resource,
        WiresSkuCombinationInterfaceFactory $wiresSkuCombinationFactory,
        WiresSkuCombinationCollectionFactory $wiresSkuCombinationCollectionFactory,
        WiresSkuCombinationSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->wiresSkuCombinationFactory = $wiresSkuCombinationFactory;
        $this->wiresSkuCombinationCollectionFactory = $wiresSkuCombinationCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(
        WiresSkuCombinationInterface $wiresSkuCombination
    ) {
        try {
            $this->resource->save($wiresSkuCombination);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the wiresSkuCombination: %1',
                $exception->getMessage()
            ));
        }
        return $wiresSkuCombination;
    }

    /**
     * @inheritDoc
     */
    public function get($wiresSkuCombinationId)
    {
        $wiresSkuCombination = $this->wiresSkuCombinationFactory->create();
        $this->resource->load($wiresSkuCombination, $wiresSkuCombinationId);
        if (!$wiresSkuCombination->getId()) {
            throw new NoSuchEntityException(__('WiresSkuCombination with id "%1" does not exist.', $wiresSkuCombinationId));
        }
        return $wiresSkuCombination;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->wiresSkuCombinationCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(
        WiresSkuCombinationInterface $wiresSkuCombination
    ) {
        try {
            $wiresSkuCombinationModel = $this->wiresSkuCombinationFactory->create();
            $this->resource->load($wiresSkuCombinationModel, $wiresSkuCombination->getId());
            $this->resource->delete($wiresSkuCombinationModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the WiresSkuCombination: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($wiresSkuCombinationId)
    {
        return $this->delete($this->get($wiresSkuCombinationId));
    }
}


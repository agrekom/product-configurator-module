<?php

namespace Agrekom\ProductConfigurator\Plugin;

class AddProductConfiguratorTab
{

    protected $added = false;

    /**
     * @param \Magento\Catalog\Block\Adminhtml\Product\Attribute\Edit\Tabs $subject
     * @param $tabId
     * @param $tab
     * @return array
     * @throws \Exception
     */
    public function beforeAddTab(\Magento\Catalog\Block\Adminhtml\Product\Attribute\Edit\Tabs $subject, $tabId, $tab)
    {
        if (!$this->added) {
            $this->added = true;

            $subject->addTab(
                'add_product_configurator_tab',
                [
                    'label' => __('Product Configurator'),
                    'title' => __('Product Configurator'),
                    'content' => $subject->getChildHtml('add_product_configurator_tab'),
                    'after' => 'labels'
                ]
            );
        }

        return [$tabId, $tab];
    }

}

<?php

namespace Agrekom\ProductConfigurator\Plugin\CatalogImportExport\Model\Import;

class Product
{
    /**
     * @param  \Magento\CatalogImportExport\Model\Import\Product  $subject
     * @param $result
     * @return mixed
     */
    public function afterGetLinkNameToId(\Magento\CatalogImportExport\Model\Import\Product $subject, $result)
    {
        $result['_dependent_'] = \Agrekom\ProductConfigurator\Model\Product\Link::LINK_TYPE_DEPENDENT;
        $result['_accessory_'] = \Agrekom\ProductConfigurator\Model\Product\Link::LINK_TYPE_ACCESSORY;

        return $result;
    }
}

<?php

namespace Agrekom\ProductConfigurator\Plugin\Checkout\CustomerData\AbstractItem;

class AddCustomerConfigurationData
{

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    public function __construct(\Magento\Framework\UrlInterface $urlBuilder)
    {
        $this->urlBuilder = $urlBuilder;
    }

    public function aroundGetItemData(
        \Magento\Checkout\CustomerData\AbstractItem $subject,
        callable $proceed,
        \Magento\Quote\Model\Quote\Item $item
    )
    {
        $result = $proceed($item);

        $result['customer_configuration'] = null;
        $result['customer_configuration_remove_item_url'] = '';
        $result['customer_configuration_remove_item_title'] = '';
        $customerConfigurationId = $item->getData('customer_configuration_id');
        if ($customerConfigurationId) {
            $result['customer_configuration'] = __("Part of configuration: %1", $customerConfigurationId);
            $result['customer_configuration_remove_item_url'] = $this->urlBuilder->getUrl('checkout/cart/index');
            $result['customer_configuration_remove_item_title'] = __("Go to cart");
        }

        return $result;
    }

}

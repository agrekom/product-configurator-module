<?php

namespace Agrekom\ProductConfigurator\Plugin\Quote\Model\Item\RepresentProduct;

class SeparateSameProductInCart
{

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->request = $request;
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Item $subject
     * @param callable $proceed
     * @param \Magento\Catalog\Model\Product $product
     * @return bool
     */
    public function aroundRepresentProduct(
        \Magento\Quote\Model\Quote\Item $subject,
        callable $proceed,
        \Magento\Catalog\Model\Product $product
    )
    {
        $returnValue = $proceed($product);

        if (
            $this->request->getActionName() == \Agrekom\ProductConfigurator\Helper\Constants::ADD_TO_CART_ACTION_NAME
            AND
            $this->request->getModuleName() == \Agrekom\ProductConfigurator\Helper\Constants::MODULE_NAME
        ) {
            return false;
        }

        if ($subject->getData('customer_configuration_id') AND ($subject->getSku() == $product->getSku())) {
            return false;
        }

        return (bool) $returnValue;
    }

}

<?php

namespace Agrekom\ProductConfigurator\Setup\Patch\Data;

class AddCatalogProductLinkType
    implements \Magento\Framework\Setup\Patch\DataPatchInterface, \Magento\Framework\Setup\Patch\PatchRevertableInterface
{

    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    protected $moduleDataSetup;
    /**
     * @var \Magento\Eav\Setup\EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     * @param \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $catalogProductLinkTypeData = [
            'link_type_id' => \Agrekom\ProductConfigurator\Model\Product\Link::LINK_TYPE_DEPENDENT,
            'code' => \Agrekom\ProductConfigurator\Ui\DataProvider\Product\Form\Modifier\Dependent::DATA_SCOPE_DEPENDENT
        ];

        $this->moduleDataSetup->getConnection()->insertOnDuplicate(
            $this->moduleDataSetup->getTable('catalog_product_link_type'),
            $catalogProductLinkTypeData
        );

        $catalogProductLinkAttributeData = [
            'link_type_id' => \Agrekom\ProductConfigurator\Model\Product\Link::LINK_TYPE_DEPENDENT,
            'product_link_attribute_code' => 'position',
            'data_type' => 'int',
        ];

        $this->moduleDataSetup->getConnection()->insert(
            $this->moduleDataSetup->getTable('catalog_product_link_attribute'),
            $catalogProductLinkAttributeData
        );

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    public function revert()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

}

<?php

namespace Agrekom\ProductConfigurator\Setup\Patch\Data;

class AddGroupsByWorksDbData
    implements \Magento\Framework\Setup\Patch\DataPatchInterface, \Magento\Framework\Setup\Patch\PatchRevertableInterface
{

    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(\Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup)
    {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $connection = $this->moduleDataSetup->getConnection();
        if ($connection->isTableExists('agrekom_productconfigurator_groupsbyworks')) {
            $data = [
                [
                    'code' => 'none',
                    'title' => 'None',
                    'group_id' => 1,
                    'created_at' => date('Y-m-t H:i:s'),
                ],
                [
                    'code' => 'tynki-elewacyjne',
                    'title' => 'Tynki elewacyjne',
                    'group_id' => 1,
                    'created_at' => date('Y-m-t H:i:s'),
                ],
                [
                    'code' => 'tynki-cementowo-wapienne',
                    'title' => 'Tynki cementowo-wapienne',
                    'group_id' => 1,
                    'created_at' => date('Y-m-t H:i:s'),
                ],
                [
                    'code' => 'gipsowanie',
                    'title' => 'Gipsowanie',
                    'group_id' => 1,
                    'created_at' => date('Y-m-t H:i:s'),
                ],
                [
                    'code' => 'hydroizolacja',
                    'title' => 'Hydroizolacja',
                    'group_id' => 2,
                    'created_at' => date('Y-m-t H:i:s'),
                ],
                [
                    'code' => 'malowanie',
                    'title' => 'Malowanie',
                    'group_id' => 2,
                    'created_at' => date('Y-m-t H:i:s'),
                ],
                [
                    'code' => 'bielenie',
                    'title' => 'Bielenie',
                    'group_id' => 2,
                    'created_at' => date('Y-m-t H:i:s'),
                ],
                [
                    'code' => 'opryski',
                    'title' => 'Opryski',
                    'group_id' => 2,
                    'created_at' => date('Y-m-t H:i:s'),
                ],
                [
                    'code' => 'gruntowanie',
                    'title' => 'Gruntowanie',
                    'group_id' => 2,
                    'created_at' => date('Y-m-t H:i:s'),
                ],
                [
                    'code' => 'pianowanie',
                    'title' => 'Pianowanie',
                    'group_id' => 2,
                    'created_at' => date('Y-m-t H:i:s'),
                ],
            ];

            foreach ($data as $arr) {
                $connection->insert('agrekom_productconfigurator_groupsbyworks', $arr);
            }
        }

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    public function revert()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

}

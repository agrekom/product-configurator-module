<?php

namespace Agrekom\ProductConfigurator\Setup\Patch\Data;

class PopulateComparisonTableTitleDefaultValue
    implements \Magento\Framework\Setup\Patch\DataPatchInterface, \Magento\Framework\Setup\Patch\PatchRevertableInterface
{

    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * @var \Agrekom\ProductConfigurator\Api\StepsRepositoryInterface
     */
    protected $stepsRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     * @param \Agrekom\ProductConfigurator\Api\StepsRepositoryInterface $stepsRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Agrekom\ProductConfigurator\Api\StepsRepositoryInterface $stepsRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->stepsRepository = $stepsRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $searchCriteria = $this->searchCriteriaBuilder->create();
        $steps = $this->stepsRepository->getList($searchCriteria);
        if ($steps->getTotalCount()) {
            foreach ($steps->getItems() as $step) {
                $step->setComparisonTableTitle('comparison_table');
                $this->stepsRepository->save($step);
            }
        }

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    public function revert()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

}

<?php

namespace Agrekom\ProductConfigurator\Ui\Component\GroupsByWorks\Listing;

class DataProvider extends \Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider
{

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Magento\Framework\Api\Search\ReportingInterface $reporting,
        \Magento\Framework\Api\Search\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        array $meta = [],
        array $data = []
    )
    {
        parent::__construct(
            $name, $primaryFieldName, $requestFieldName, $reporting, $searchCriteriaBuilder, $request,
            $filterBuilder, $meta, $data
        );
    }

    /**
     * @return \Magento\Framework\Api\Search\SearchCriteria
     */
    public function getSearchCriteria()
    {
        $filer = $this->filterBuilder
            ->setField('code')
            ->setValue('none')
            ->setConditionType('neq')
            ->create();

        $this->searchCriteriaBuilder->addFilter($filer);

        return parent::getSearchCriteria();
    }

}

<?php

namespace Agrekom\ProductConfigurator\Ui\DataProvider\Product\Form\Modifier;

class Dependent extends \Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\Related
{

    const DATA_SCOPE_DEPENDENT = 'dependent';

    /**
     * @var string
     */
    protected static $previousGroup = 'search-engine-optimization';

    /**
     * @var int
     */
    protected static $sortOrder = 90;

    /**
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        $meta = array_replace_recursive(
            $meta,
            [
                static::GROUP_RELATED => [
                    'children' => [
                        $this->scopePrefix . static::DATA_SCOPE_DEPENDENT => $this->getDependentFieldset()
                    ],
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __('Related Products, Up-Sells, Cross-Sells, Accessories and Product Configurator Products'),
                                'collapsible' => true,
                                'componentType' => \Magento\Ui\Component\Form\Fieldset::NAME,
                                'dataScope' => static::DATA_SCOPE,
                                'sortOrder' => $this->getNextGroupSortOrder(
                                    $meta,
                                    self::$previousGroup,
                                    self::$sortOrder
                                ),
                            ],
                        ],
                    ],
                ],
            ]
        );
        return $meta;
    }

    /**
     * @return array
     */
    protected function getDependentFieldset()
    {
        $content = __(
            'Product configurator dependent products used for product configuration process.'
        );

        return [
            'children' => [
                'button_set' => $this->getButtonSet(
                    $content,
                    __('Add Product Configurator Dependent Products'),
                    $this->scopePrefix . static::DATA_SCOPE_DEPENDENT
                ),
                'modal' => $this->getGenericModal(
                    __('Add Product Configurator Dependent Products'),
                    $this->scopePrefix . static::DATA_SCOPE_DEPENDENT
                ),
                static::DATA_SCOPE_DEPENDENT => $this->getGrid($this->scopePrefix . static::DATA_SCOPE_DEPENDENT),
            ],
            'arguments' => [
                'data' => [
                    'config' => [
                        'additionalClasses' => 'admin__fieldset-section',
                        'label' => __('Product Configurator Dependent Products'),
                        'collapsible' => false,
                        'componentType' => \Magento\Ui\Component\Form\Fieldset::NAME,
                        'dataScope' => '',
                        'sortOrder' => 90,
                    ],
                ],
            ]
        ];
    }

    /**
     * @return array
     */
    protected function getDataScopes()
    {
        return [
            static::DATA_SCOPE_DEPENDENT
        ];
    }

}

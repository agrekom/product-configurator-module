<?php

namespace Agrekom\ProductConfigurator\Ui\DataProvider\Product\Related;

class AccessoryDataProvider extends \Magento\Catalog\Ui\DataProvider\Product\Related\AbstractDataProvider
{

    /**
     * {@inheritdoc
     */
    protected function getLinkType()
    {
        return 'accessory';
    }

}

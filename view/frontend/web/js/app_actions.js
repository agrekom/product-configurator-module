require(['jquery'], function ($) {
  app_actions = (function () {
    var init, actions, chooseProduct, chooseAccessory, chooseRelatedItem, isEmail, sendViaEmail;

    actions = {};

    chooseProduct = function (productId) {
      helpers.selectFromApp('.js-alert').addClass('is-hidden');
      var isProductAdded = app_data.addProduct(productId);

      if (isProductAdded.success) {
        var pageLayout = app_data.getCurrentStepData().step_page_layout;

        if (pageLayout !== 'purpose') {
          templates.clearProductsWithRelatedAndAccessories();

          /* if addProduct return true product was added successfully */
          templates.showProductAccessories(productId);

          /**
           * related items excluded from step_30 (zbiorniki agregatu)
           */
          if (app_data.state.currentStep.name !== 'step_30') {
            var relatedItems = app_data.getProductRelatedItems(productId);

            if (relatedItems) {
              /**
               * adding first item in the list as related
               * first page layouts will not have raleted products displayed
               */
              relatedItems = app_data.isContainerSKUFilter(relatedItems);

              if (relatedItems.length > 0) {
                app_data.addRelatedItem(relatedItems[0].id, productId);
              }
            }
          }
        }
        templates.updateCurrentStep({
          clicked: true
        });
      } else if (!isProductAdded.success) {
        app_data.clearProductsWithRelatedAndAccessories();
        templates.clearProductsWithRelatedAndAccessories();
      } else {
        templates.clearProductsWithRelatedAndAccessories();
      }
    };

    chooseAccessory = function (productId, parentId) {
      app_data.addAccessory(productId, parentId);
      templates.updateCurrentStep();
    };

    chooseRelatedItem = function (optionProductId, parentId) {
      app_data.addRelatedItem(optionProductId, parentId);
      templates.updateItemsImg();
      templates.updatePriceDisplay();
    };

    isEmail = function (email) {
      var regex =
        /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
      return regex.test(email);
    };

    sendViaEmail = function (config) {
      var inputValue = helpers.selectFromApp(config.emailInput).val();
      var useThisTranslation = translations.pleaseProvideEmailOrPhone;
      if (config.sendEmail) {
        useThisTranslation = translations.pleaseProvideEmail;
      }

      if (inputValue) {
        if (isEmail(inputValue) || /\d/.test(inputValue)) {
          $.ajax({
            method: config.method,
            url: config.url,
            data: {
              notification: JSON.stringify(app_data.getSummaryDataToSend(inputValue, config.sendEmail))
            }
          }).done(function (msg) {
            helpers
              .selectFromApp(config.displayError)
              .html(`<span style="color: green">&#x2714; ${translations.notificationSent}</span>`);
          });
        } else if (inputValue.includes('@')) {
          helpers.selectFromApp(config.displayError).html(translations.invalidEmail);
        } else {
          helpers.selectFromApp(config.displayError).html(useThisTranslation);
        }
      } else {
        helpers.selectFromApp(config.displayError).html(translations.useThisTranslation);
      }
    };

    actions.nextStep = function () {
      helpers.delegateClickToContainer('js-show-next-step', function () {
        if (app_data.checkIfStepIsValid() || app_data.state.currentStep.isProductAlreadyChosenInPreviousSteps) {
          app_data.stepUpdateGroupsInformation();
          app_data.setNextStep();
          templates.showCurrentStep();
        } else if (app_data.state.currentStep.isMultipleProducts) {
          helpers.displayAlert(translations.alertChooseMultiple);
        } else {
          helpers.displayAlert(translations.alertChooseSingle);
        }
        helpers.scrollToTop();
      });
    };

    actions.previousStep = function () {
      helpers.delegateClickToContainer('js-show-previous-step', function () {
        app_data.setPreviousStep();
        templates.showCurrentStep({ previousStep: true });
        helpers.scrollToTop();
      });
    };

    actions.goToLastStep = function () {
      helpers.delegateClickToContainer('js-show-last-step', function () {
        app_data.stepUpdateGroupsInformation();
        app_data.setLastStep();
        templates.showCurrentStep();
      });
    };

    actions.mockupSwitchBtn = function () {
      helpers.delegateClickToContainer('js-mockup-switch', function () {
        if (mockupSwitch) {
          mockupSwitch = false;
          $('.js-mockup-switch').html('&#x2717; MOCKUP SWITCH');
        } else {
          mockupSwitch = true;
          $('.js-mockup-switch').html('&#x2713; MOCKUP SWITCH');
        }
      });
    };

    actions.addProduct = function () {
      helpers.delegateClickToContainer('js-add-product', function ($addProduct) {
        var productId = $addProduct.attr('data-product-id');

        chooseProduct(productId);
      });
    };

    actions.addAccessory = function () {
      helpers.delegateClickToContainer(
        'js-add-accessory',
        function ($addProduct) {
          var productId = $addProduct.attr('data-product-id'),
            parentId = $addProduct.attr('data-parent-id');

          chooseAccessory(productId, parentId);
        },
        true
      );
    };

    actions.addRelatedItem = function () {
      /* stop propagation on select click */
      helpers.delegateClickToContainer('js-related-items-select', function () {}, true);
      helpers.delegateClickToContainer('js-related-items-select-mobile', function () {}, true);
      $('body').on('change', '.js-related-items-select', function (event) {
        var optionProductId = helpers.selectFromApp('.js-related-items-select option:selected').attr('value'),
          parentId = helpers.selectFromApp('.js-related-items-select').attr('data-parent-id');

        helpers.selectFromApp('.js-related-items-select-mobile').val(optionProductId);
        chooseRelatedItem(optionProductId, parentId);
      });
      $('body').on('change', '.js-related-items-select-mobile', function (event) {
        var optionProductId = helpers.selectFromApp('.js-related-items-select-mobile option:selected').attr('value'),
          parentId = helpers.selectFromApp('.js-related-items-select-mobile').attr('data-parent-id');

        helpers.selectFromApp('.js-related-items-select').val(optionProductId);
        chooseRelatedItem(optionProductId, parentId);
      });
    };

    actions.showCompareTable = function () {
      helpers.delegateClickToContainer('js-show-compare-table', function () {
        helpers.selectFromApp('.js-compare-table').removeClass('is-hidden');
        templates.updateAttributesHeight();
        $('body').addClass('no-scroll');
      });
      helpers.delegateClickToContainer('js-compare-table-close', function () {
        helpers.selectFromApp('.js-compare-table').addClass('is-hidden');
        $('body').removeClass('no-scroll');
      });
    };

    actions.closeAlert = function () {
      helpers.delegateClickToContainer('js-alert-close', function () {
        helpers.selectFromApp('.js-alert').addClass('is-hidden');
      });
    };

    actions.sendChoicesNotification = function () {
      var configForNotification = {
        emailInput: '.js-notification-email',
        displayError: '.js-notification-error',
        method: 'GET',
        url: '/product-configurator/notificationemail/send',
        sendEmail: false
      };
      helpers.delegateClickToContainer('js-send-notification', sendViaEmail.bind(null, configForNotification));
    };

    actions.sendConfigurationEmail = function () {
      var configsSendConfigurationEmail = {
        emailInput: '.js-send-configuration-email',
        displayError: '.js-send-configuration-error',
        method: 'GET',
        url: '/product-configurator/customerconfiguration/save',
        sendEmail: true
      };
      helpers.delegateClickToContainer('js-send-configuration', sendViaEmail.bind(null, configsSendConfigurationEmail));
    };

    actions.validationForNotificationEmail = function () {
      $('body').on('change', '.js-notification-email', function () {
        helpers.selectFromApp('.js-notification-error').html('');
      });
    };

    actions.addChoicesToCart = function () {
      helpers.delegateClickToContainer('js-add-to-cart', function () {
        var $addToCartForm = helpers.selectFromApp('.js-summary-add-to-cart-form'),
          $addToCartInput = helpers.selectFromApp('.js-summary-add-to-cart-data'),
          dataToSend = app_data.getSummaryDataToSend();

        $addToCartInput.val(JSON.stringify(dataToSend));
        $addToCartForm.submit();
      });
    };

    actions.showIfNextStepIsValid = function () {
      helpers.delegateClickToContainer('js-show-if-next-step-is-valid', function () {
        console.log(app_data.checkForNextValidStep());
        console.log(app_data.state.allAvailableSteps);
      });
    };

    actions.closeOverlay = function () {
      helpers.delegateClickToContainer('js-close-overlay', function () {
        helpers.selectFromApp('.js-footer-overlay').addClass('hidden');
        helpers.selectFromApp('.js-footer-left-column').addClass('hidden');
      });
    };

    actions.openOverlay = function () {
      helpers.delegateClickToContainer(
        'js-change-related-btn',
        function () {
          helpers.selectFromApp('.js-footer-overlay').removeClass('hidden');
          helpers.selectFromApp('.js-footer-left-column').removeClass('hidden');
        },
        true
      );
    };

    actions.toggleAccessoriesPopup = function () {
      helpers.delegateClickToContainer('js-hide-footer-content', function () {
        helpers.selectFromApp('.js-footer-content-column').addClass('hidden');
        helpers.selectFromApp('.js-show-footer-content').removeClass('hidden');
      });
      helpers.delegateClickToContainer('js-show-footer-content', function () {
        helpers.selectFromApp('.js-footer-content-column').removeClass('hidden');
        helpers.selectFromApp('.js-show-footer-content').addClass('hidden');
      });
    };

    actions.toggleSendConfigurationPopup = function () {
      helpers.delegateClickToContainer('js-hide-send-configuration-popup', function () {
        helpers.selectFromApp('.js-summary-actions-popup').addClass('hidden');
      });
      helpers.delegateClickToContainer('js-show-send-configuration-popup', function () {
        helpers.selectFromApp('.js-summary-actions-popup').removeClass('hidden');
      });
    };

    init = function () {
      actions.nextStep();
      actions.previousStep();
      actions.goToLastStep();
      actions.addProduct();
      actions.addAccessory();
      actions.showCompareTable();
      actions.addRelatedItem();
      actions.sendChoicesNotification();
      actions.sendConfigurationEmail();
      actions.validationForNotificationEmail();
      actions.addChoicesToCart();
      actions.showIfNextStepIsValid();
      actions.mockupSwitchBtn();
      actions.closeAlert();
      actions.closeOverlay();
      actions.openOverlay();
      actions.toggleAccessoriesPopup();
      actions.toggleSendConfigurationPopup();

      $(window).on('resize', helpers.sliderTogglerForDevices);
    };

    return {
      init: init
    };
  })();
});

require(['jquery'], function ($) {
  app_data = (function () {
    var init,
      state,
      populate,
      checkIfProductHasUniqueGroup,
      stepUpdateGroupsInformation,
      deleteGroupsWhenNavigatingToPreviousStep,
      isContainerSKUFilter,
      getAllChoicesSKUArray,
      isRelatedItemFromGroupAlreadyChosen,
      calculatePrice,
      addProduct,
      addAccessory,
      addRelatedItem,
      clearAccessoriesAndRelatedFromChoices,
      createStepsNames,
      isProductAlreadyChosen,
      isProductAlreadyChosenInPreviousSteps,
      setCurrentStep,
      setNextStep,
      setPreviousStep,
      setLastStep,
      getValidStepNumber,
      getCurrentStepData,
      getSpecificStepData,
      getProductFromCurrentStep,
      getProductRelatedItems,
      getSummaryData,
      getProductsId,
      getDependentProductIds,
      findAllAvailableSteps,
      findProductsToShowBasedOnDependentIds,
      checkShowOnlyForPurposeGroup,
      checkIfNextStepIsValid,
      checkIfNextStepIsValidBasedOnPurpose,
      checkForNextValidStep,
      checkIfStepIsValid,
      clearProductsWithRelatedAndAccessories,
      emptyProductCheck,
      getSummaryDataToSend;

    state = {
      initData: {},
      steps: {},
      currentStep: {
        index: 0,
        name: '',
        isMultipleProducts: false,
        isProductAlreadyChosenInPreviousSteps: false
      },
      stepMarker: 1,
      allAvailableSteps: {},
      choices: {},
      groups: {}
    };

    addProduct = function (productId) {
      var productToAdd = getProductFromCurrentStep(productId);

      if (productToAdd) {
        var chosenStepProducts = state.choices[app_data.state.currentStep.name].products,
          isProductChosen = isProductAlreadyChosen(chosenStepProducts, productId);

        if (isProductChosen.found) {
          chosenStepProducts.splice(isProductChosen.index, 1);
          console.info('%cproduct removed! id: ' + productId, helpers.consoleProductRemovedColor());
          return { success: false };
        } else {
          /* switch between multiple products choice or single choice */
          var isMultipleProducts = state.currentStep.isMultipleProducts;
          if (isMultipleProducts) {
            chosenStepProducts.push(productToAdd);
          } else {
            chosenStepProducts.splice(0, chosenStepProducts.length);
            chosenStepProducts.push(productToAdd);

            /**
             * New product added so we clear choices related to old product chosen
             */
            var chosenAccessories = state.choices[app_data.state.currentStep.name].accessories,
              chosenRelated = state.choices[app_data.state.currentStep.name].related;

            clearAccessoriesAndRelatedFromChoices(chosenAccessories, chosenRelated);
          }
          console.info('%cproduct added successfully id: ' + productId, helpers.consoleProductAddedColor());
          return { success: true };
        }
      }
      return false;
    };

    addAccessory = function (productId, parentId) {
      var parentProduct = getProductFromCurrentStep(parentId);

      if (parentProduct) {
        var chosenStepProducts = state.choices[app_data.state.currentStep.name].accessories,
          parentProductAccessories = Object.values(parentProduct.product_accessories),
          isProductChosen = isProductAlreadyChosen(chosenStepProducts, productId);

        if (isProductChosen.found) {
          chosenStepProducts.splice(isProductChosen.index, 1);
          console.info('%cproduct removed! ' + productId, helpers.consoleProductRemovedColor());
          return;
        } else {
          var accessoryToAdd;
          parentProductAccessories.forEach(function (item) {
            if (item.id === productId) {
              accessoryToAdd = item;
            }
          });
          chosenStepProducts.push(accessoryToAdd);
          console.info('%caccessory added successfully: ' + productId, helpers.consoleProductAddedColor());
          return true;
        }
      }
      return false;
    };

    addRelatedItem = function (productId, parentId) {
      var parentProduct = getProductFromCurrentStep(parentId);

      if (parentProduct) {
        var chosenStepProducts,
          parentProductRelatedItems = getProductRelatedItems(parentId),
          groupByWork = checkIfProductHasUniqueGroup(parentId);

        if (parentProductRelatedItems) {
          var relatedItemToAdd;
          parentProductRelatedItems.forEach(function (item) {
            if (item.id === productId) {
              relatedItemToAdd = item;
            }
          });

          /* creating information about new groups */
          if (isRelatedItemFromGroupAlreadyChosen(parentId)) {
            return false;
          }

          /* clear all groups - only one related product per step is allowed */
          state.choices[app_data.state.currentStep.name].related = {};

          if (groupByWork.group !== 0) {
            state.choices[app_data.state.currentStep.name].related[groupByWork.group] = [];
            chosenStepProducts = state.choices[app_data.state.currentStep.name].related[groupByWork.group];
          } else {
            state.choices[app_data.state.currentStep.name].related[0] = [];
            chosenStepProducts = state.choices[app_data.state.currentStep.name].related[0];
          }

          chosenStepProducts.push(relatedItemToAdd);
          console.info(
            '%cnew related item added successfully ' + relatedItemToAdd.id,
            helpers.consoleProductAddedColor()
          );
          return true;
        }
      }
      return false;
    };

    clearAccessoriesAndRelatedFromChoices = function (chosenAccessories, chosenRelated) {
      if (chosenAccessories.length > 0) {
        chosenAccessories.splice(0, chosenAccessories.length);
      }

      if (chosenRelated > 0) {
        chosenRelated.splice(0, chosenRelated.length);
      }
    };

    checkIfProductHasUniqueGroup = function (parentId) {
      var parentProduct = getProductFromCurrentStep(parentId);

      if (parentProduct && parentProduct.group_by_work !== null) {
        return parentProduct.group_by_work;
      }
      return {
        code: 'no_group',
        group: 0
      };
    };

    stepUpdateGroupsInformation = function () {
      var relatedChoices = Object.keys(state.choices[app_data.state.currentStep.name].related);
      relatedChoices.forEach((key) => {
        state.groups[key] = {
          isProductChosen: true,
          step: app_data.state.currentStep.name
        };
        console.info(
          '%cupdated information for group: ' + key,
          'display: inline-block; padding: 6px; background-color: OrangeRed'
        );
      });
    };

    deleteGroupsWhenNavigatingToPreviousStep = function () {
      var currentStep = app_data.state.currentStep;

      Object.entries(app_data.state.groups).forEach(([key, group]) => {
        if (group.step === currentStep.name) {
          delete app_data.state.groups[key];
          console.info('%cdestroyed group: ' + key, 'display: inline-block; padding: 6px; background-color: DarkRed');
        }
      });
    };

    getAllChoicesSKUArray = function (showAlerts = false) {
      var allChoices = Object.values(state.choices),
        nullableObject = false,
        allChoicesSKUArray = [],
        allChoicesSKUArrayOfArrays = [],
        isUniquePurposeChosen = false,
        isUniqueContainerChosen = false;

      allChoices.forEach((stepChoice) => {
        stepChoice.products.forEach((product) => {
          var isContainer = Boolean(Number.parseInt(product.is_container_product)),
            isPurpose = Boolean(Number.parseInt(product.is_purpose_product));

          if (isPurpose && Number.parseInt(product.id) === 203) {
            isUniquePurposeChosen = true;
          }

          if (isContainer && product.sku === 'AG60DWC') {
            isUniqueContainerChosen = true;
          }

          /**
           * Used isPurpose here before
           * maybe to uncomment or delete later
           * isPurpose = Boolean(Number.parseInt(product.is_purpose_product));
           * if (isContainer || isPurpose) {
           */
          if (isContainer) {
            if (Boolean(product.dlugosc_przewodow_konfigurator) && !nullableObject) {
              var productWiresSKUArray = product.dlugosc_przewodow_konfigurator.split(',');
              /**
               * Creating array of arrays for all product choices
               * that are container or purpose
               */
              allChoicesSKUArrayOfArrays.push(productWiresSKUArray);
            } else if (!nullableObject) {
              allChoicesSKUArrayOfArrays.length = 0;
              nullableObject = true;
            }
          }
        });
      });

      if (allChoicesSKUArrayOfArrays.length > 0) {
        /**
         * Finding intersection for multiple SKU arrays
         */
        allChoicesSKUArray = helpers.findArraysIntersection(allChoicesSKUArrayOfArrays);
        allChoicesSKUArray = helpers.deduplicateArray(allChoicesSKUArray);
      }

      /**
       * filtering related items for "tynki cementowo wapienne"
       * including SKU hardcoded using
       * isUniqueContainerChosen and isUniquePurposeChosen
       */
      var dependentFromPurposeProductId = Number.parseInt(state.currentStep.dependentFromPurposeProductId);
      if (isUniquePurposeChosen && isUniqueContainerChosen && dependentFromPurposeProductId == 203) {
        //allChoicesSKUArray = allChoicesSKUArray.filter((sku) => ['W5MBT25', 'W10MBT25', 'W15MBT25'].includes(sku));
      }

      /**
       * Just showing alerts for debugging
       */
      if (showAlerts) {
        if (nullableObject) {
          console.info(
            '%cnull item for SKU container or purpose chosen! no possible configuration available',
            'display: inline-block; padding: 6px; background-color: DarkRed'
          );
        }
        var infoSKUArray = '';
        allChoicesSKUArray.forEach((sku) => {
          infoSKUArray += sku + ' ';
        });

        if (infoSKUArray) {
          console.info('%csku items possible to show: ' + infoSKUArray, helpers.consoleInfoColor2());
        } else {
          console.info('%cno sku items to show', helpers.consoleInfoColor2());
        }
      }

      return allChoicesSKUArray;
    };

    isContainerSKUFilter = function (relatedProductsArrayToFilter, showAlerts = false) {
      var filteredRelatedProductsArray = [],
        productsWiresSKUArray = getAllChoicesSKUArray(showAlerts),
        infoString = '%crelated products (wires SKU) can be displayed:',
        infoStringNotDisplayed = '%crelated products (wires SKU) hidden:',
        toShow = [],
        toHide = [];

      if (relatedProductsArrayToFilter.length > 0) {
        relatedProductsArrayToFilter.forEach((relatedProduct) => {
          productsWiresSKUArray.forEach((parentWireSKU) => {
            if (Boolean(relatedProduct.sku) && Boolean(parentWireSKU)) {
              if (parentWireSKU === relatedProduct.sku) {
                infoString += ' ' + relatedProduct.sku;
                filteredRelatedProductsArray.push(relatedProduct);
                toShow.push(relatedProduct.sku);
              } else {
                toHide.push(relatedProduct.sku);
              }
            }
          });
        });

        toShow = helpers.deduplicateArray(toShow);
        toHide = helpers.deduplicateArray(toHide);

        toHide = toHide.filter((sku) => !toShow.includes(sku));

        if (showAlerts && toHide.length > 0) {
          toHide.forEach((item) => {
            infoStringNotDisplayed += ' ' + item;
          });

          console.info(infoStringNotDisplayed, 'display: inline-block; padding: 6px; background-color: DarkRed');
        }

        if (showAlerts && toShow.length > 0) {
          console.info(infoString, 'display: inline-block; padding: 6px; background-color: OrangeRed');
        }

        return filteredRelatedProductsArray;
      }

      return relatedProductsArrayToFilter;
    };

    isRelatedItemFromGroupAlreadyChosen = function (parentId, showAlert = true, switchNoGroup = false) {
      var itemGroup = checkIfProductHasUniqueGroup(parentId);

      if (itemGroup.group !== 0) {
        if (state.groups[itemGroup.group] && state.groups[itemGroup.group].isProductChosen) {
          if (showAlert) {
            state.choices[state.currentStep.name].related = {};
            console.info(
              '%cgroup related product has been already chosen for: ' + itemGroup.group + ' related select is hidden',
              helpers.consoleInfoColor2()
            );
          }
          return true;
        }
      }

      /* switchNoGroup if no_group [0] should also be not visible when product is added */
      if (switchNoGroup) {
        if (state.groups[0] && state.groups[0].isProductChosen) {
          return true;
        }
      }
      if (showAlert) {
        console.info(
          '%crelated product available to add to empty group: ' + itemGroup.group,
          helpers.consoleInfoColor()
        );
      }
      return false;
    };

    populate = function (data) {
      state.initData = data;
      createStepsNames(state.initData);
    };

    calculatePrice = function () {
      if (state.choices) {
        var priceSum = 0,
          allChoices = Object.values(state.choices);

        allChoices.forEach((stepChoice) => {
          stepChoice.products.forEach((product) => {
            priceSum += product.raw_price_with_tax;
          });

          stepChoice.accessories.forEach((product) => {
            priceSum += product.raw_price_with_tax;
          });

          if (Array.isArray(Object.values(stepChoice.related))) {
            Object.values(stepChoice.related).forEach((relatedItems) => {
              relatedItems.forEach((product) => {
                priceSum += product.raw_price_with_tax;
              });
            });
          }
        });

        return priceSum;
      }
      return;
    };

    createStepsNames = function (data) {
      state.steps = Object.keys(data);
      /* setting first step */
      setCurrentStep(0);
    };

    setCurrentStep = function (index) {
      state.currentStep.index = index;
      state.currentStep.name = state.steps[index];
      state.currentStep.isMultipleProducts = false;
      state.currentStep.isProductAlreadyChosenInPreviousSteps = false;
      state.currentStep.dependentFromPurposeProductId =
        state.initData[state.currentStep.name].dependent_from_purpose_product_id;

      /* setting step chosen products & accessories arrays */
      if (!state.choices[app_data.state.currentStep.name]) {
        state.choices[app_data.state.currentStep.name] = {
          products: [],
          accessories: [],
          related: {}
        };
      }

      /* setting multiple choice for step */
      if (state.currentStep.index === 0) {
        state.currentStep.isMultipleProducts = true;
      }
    };

    setNextStep = function () {
      var nextValidStep = checkForNextValidStep();

      if (nextValidStep) {
        if (nextValidStep.index > state.steps.length - 1) {
          return false;
        }
        state.stepMarker += 1;
        setCurrentStep(nextValidStep.index);
      } else if (state.currentStep.index > 0 && !nextValidStep) {
        state.stepMarker += 1;
        setCurrentStep(state.steps.length - 1);
      }
      return false;
    };

    setPreviousStep = function () {
      var previousStepToShow = {
        index: 0,
        name: 'step_10'
      };
      state.allAvailableSteps.forEach((availableStep) => {
        if (!(availableStep.index >= state.currentStep.index)) {
          previousStepToShow = {
            index: availableStep.index,
            name: availableStep.name
          };
        }
      });

      delete state.choices[state.currentStep.name];
      state.stepMarker -= 1;
      setCurrentStep(previousStepToShow.index);
    };

    setLastStep = function () {
      setCurrentStep(state.steps.length - 1);
    };

    getValidStepNumber = function (index) {
      if (index < 0) {
        return false;
      }

      if (index > state.steps.length - 1) {
        return false;
      }

      return index;
    };

    getCurrentStepData = function () {
      return state.initData[state.currentStep.name];
    };

    getSpecificStepData = function (stepName) {
      return state.initData[stepName];
    };

    isProductAlreadyChosen = function (chosenProductsArray, productIdToFind) {
      var productFound = { index: 0, found: false };
      if (chosenProductsArray.length > 0) {
        chosenProductsArray.forEach(function (product, index) {
          if (productIdToFind === product.id) {
            productFound.found = true;
            productFound.index = index;
          }
        });
      }
      return productFound;
    };

    isProductAlreadyChosenInPreviousSteps = function (productIdToFind) {
      var productFound = false;
      Object.entries(state.choices).forEach(([stepName, choices]) => {
        if (stepName === state.currentStep.name || stepName === 'step_10' || stepName === 'step_20') {
          return false;
        } else {
          var isProductChosen = isProductAlreadyChosen(choices.products, productIdToFind);
          if (isProductChosen.found) {
            productFound = true;
            state.currentStep.isProductAlreadyChosenInPreviousSteps = true;
          }
        }
      });
      return productFound;
    };

    getProductFromCurrentStep = function (productIdToFind) {
      var currentProducts = Object.values(getCurrentStepData().step_products),
        productToReturn;

      currentProducts.forEach(function (product) {
        if (productIdToFind === product.id) {
          productToReturn = product;
        }
      });
      if (productToReturn) {
        return productToReturn;
      }
      return;
    };

    getProductRelatedItems = function (productId) {
      if (productId) {
        var product = getProductFromCurrentStep(productId),
          productRelatedItemsArray = [];

        if (product.left_col_related_products !== null) {
          /* array [0] refers to 'wires' */
          productRelatedItemsArray = Object.values(product.left_col_related_products)[0];
        }

        if (productRelatedItemsArray.length) {
          return productRelatedItemsArray;
        } else {
          return false;
        }
      } else {
        return false;
      }
    };

    getSummaryData = function () {
      return state.choices;
    };

    getProductsId = function (productsArray) {
      var productIds = [];
      Object.values(productsArray).forEach((product) => {
        productIds.push(product.id);
      });
      return productIds;
    };

    getDependentProductIds = function (productsArray) {
      var dependantProductIds = [];
      Object.values(productsArray).forEach((product) => {
        var arrayToMerge = Object.values(product.dependent_product_ids);
        dependantProductIds = helpers.mergeAndDeduplicateArrays(dependantProductIds, arrayToMerge);
      });
      return dependantProductIds;
    };

    findProductsToShowBasedOnDependentIds = function (productsToCheck) {
      var chosenPurposeId = [],
        chosenPurposeGroups = [];

      if (state.choices.step_10.products) {
        Object.values(state.choices.step_10.products).forEach(function (purposeItem) {
          chosenPurposeId.push(purposeItem.id);
          if (purposeItem.group_by_work) {
            chosenPurposeGroups.push(Number.parseInt(purposeItem.group_by_work.group));
          } else {
            chosenPurposeGroups.push(0);
          }
        });

        chosenPurposeGroups = helpers.deduplicateArray(chosenPurposeGroups);
      }

      var productsToShow = [];
      if (productsToCheck.length > 0) {
        productsToCheck.forEach((product) => {
          /**
           * Exception from dependent_product_ids
           * to display product based on chosen purpose
           */
          var showOnlyIfPurposeProductIsChosenArr = [null, 0, '0'];
          if (showOnlyIfPurposeProductIsChosenArr.includes(product.show_only_if_purpose_product_is_chosen) === false) {
            var purposeException = product.show_only_if_purpose_product_is_chosen;

            if (chosenPurposeId.includes(purposeException)) {
              productsToShow.push(product);
            }
          } else {
            /**
             * Standard check depending ids path
             */
            var findDependingIds = Object.values(product.dependent_product_ids).filter(function (n) {
              return chosenPurposeId.indexOf(n) !== -1;
            });

            if (
              findDependingIds.length > 0 &&
              state.currentStep.name !== 'step_10' &&
              checkShowOnlyForPurposeGroup(chosenPurposeGroups, product)
            ) {
              if (chosenPurposeId.length > 0 && Boolean(Number.parseInt(product.is_container_product))) {
                var containsIds = chosenPurposeId.every((purposeId) => {
                  return findDependingIds.includes(purposeId);
                });
                if (containsIds) {
                  productsToShow.push(product);
                }
              } else {
                productsToShow.push(product);
              }
            }
          }
        });
      }
      if (productsToShow.length > 0) {
        return productsToShow;
      }
      return false;
    };

    checkShowOnlyForPurposeGroup = function (chosenPurposeGroups, product) {
      var showOnlyForPurposeGroup = Boolean(product.show_only_for_purpose_group)
          ? Number.parseInt(product.show_only_for_purpose_group)
          : 0,
        isInGroup = chosenPurposeGroups.some((purposeGroup) => purposeGroup === showOnlyForPurposeGroup);

      if (showOnlyForPurposeGroup === 0 || (isInGroup && chosenPurposeGroups.length < 2)) {
        return true;
      }
      return false;
    };

    checkIfNextStepIsValid = function () {
      var nextStepProducts = false,
        nextIndex = getValidStepNumber(state.currentStep.index + 1);

      if (nextIndex) {
        nextStepProducts = getSpecificStepData(state.steps[nextIndex]).step_products;
      }

      var chosenProductsId = [];
      Object.values(state.choices).forEach(function (stepItem) {
        var stepProductsIds = getProductsId(stepItem.products);
        chosenProductsId = helpers.mergeAndDeduplicateArrays(chosenProductsId, stepProductsIds);
      });

      if (nextStepProducts) {
        var dependantProductIds = getDependentProductIds(nextStepProducts);
        if (dependantProductIds.length > 0) {
          return { dependantProductIds, chosenProductsId };
        }
      }
      return false;
    };

    checkIfNextStepIsValidBasedOnPurpose = function (stepIndex) {
      var nextStepProducts = false,
        validStepIndex = getValidStepNumber(stepIndex),
        chosenPurposeId = [],
        chosenPurposeGroups = [];

      if (state.choices.step_10.products) {
        Object.values(state.choices.step_10.products).forEach(function (purposeItem) {
          chosenPurposeId.push(purposeItem.id);
          if (purposeItem.group_by_work) {
            chosenPurposeGroups.push(Number.parseInt(purposeItem.group_by_work.group));
          } else {
            chosenPurposeGroups.push(0);
          }
        });

        chosenPurposeGroups = helpers.deduplicateArray(chosenPurposeGroups);
      }

      if (validStepIndex) {
        var stepDataToValidate = getSpecificStepData(state.steps[validStepIndex]),
          stepDependencyFromPurpose = stepDataToValidate.dependent_from_purpose_product_id;

        /**
         * We show first 3 steps: purpose, step_20 and step_30 then we validate
         * with dependency on chosen purpose
         */

        var boolValidatorArr = [null, 0, '0'];
        if (!boolValidatorArr.includes(stepDependencyFromPurpose)) {
          if (!chosenPurposeId.includes(stepDependencyFromPurpose) && ![0, 1, 2].includes(stepIndex)) {
            return false;
          }
        }

        nextStepProducts = stepDataToValidate.step_products;
      }

      if (nextStepProducts) {
        var nextStepProducts = nextStepProducts.filter(function (product) {
            return checkShowOnlyForPurposeGroup(chosenPurposeGroups, product);
          }),
          dependantProductIds = getDependentProductIds(nextStepProducts);

        if (dependantProductIds.length > 0) {
          var findDependingIds = dependantProductIds.filter(function (n) {
            return chosenPurposeId.indexOf(n) !== -1;
          });
          if (findDependingIds.length > 0) {
            return findDependingIds;
          }
          return false;
        }
      }
      return false;
    };

    checkForNextValidStep = function () {
      var nextValidStep = false;

      state.steps.some((stateStep, index) => {
        if (index > state.currentStep.index) {
          var isStepValid = checkIfNextStepIsValidBasedOnPurpose(index);
          if (isStepValid.length > 0) {
            nextValidStep = { index, name: stateStep };
            return true;
          }
        }
      });

      return nextValidStep;
    };

    findAllAvailableSteps = function () {
      /* 2 steps available always at start - purpose and summary */
      var counter = 2;
      state.allAvailableSteps = [];
      state.steps.forEach((stateStep, index) => {
        var isStepValid = checkIfNextStepIsValidBasedOnPurpose(index);
        if (isStepValid.length > 0) {
          counter += 1;
          state.allAvailableSteps.push({ index, name: stateStep });
        }
      });
      return counter;
    };

    checkIfStepIsValid = function () {
      var currentStepChoices = state.choices[state.currentStep.name].products;

      if (currentStepChoices.length > 0) {
        return true;
      }
      return false;
    };

    clearProductsWithRelatedAndAccessories = function () {
      state.choices[app_data.state.currentStep.name].accessories.length = 0;
      /* clear all groups - only one related product per step is allowed */
      state.choices[app_data.state.currentStep.name].related = {};
    };

    emptyProductCheck = function (products) {
      var emptyFound = false;

      products.forEach((product) => {
        if (product.sku.toLowerCase().startsWith('bez-')) {
          emptyFound = true;
        }
      });

      return emptyFound;
    };

    getSummaryAllProductsIds = function (summaryData) {
      var allProductIds = [];

      Object.entries(summaryData).forEach(([key, step]) => {
        if (key === 'step_10') {
          return;
        }

        if (emptyProductCheck(Object.values(step.products))) {
          return;
        }

        Object.values(step.products).forEach((product) => {
          allProductIds.push({ id: product.id, qty: 1, isWire: false });
        });

        if (Array.isArray(step.accessories)) {
          Object.values(step.accessories).forEach((product) => {
            allProductIds.push({ id: product.id, qty: 1, isWire: false });
          });
        }

        if (Array.isArray(Object.values(step.related))) {
          Object.values(step.related).forEach((relatedItems) => {
            var item = relatedItems[0];
            allProductIds.push({ id: item.id, qty: 1, isWire: true });
          });
        }
      });

      return allProductIds;
    };

    getSummaryDataToSend = function (inputValue = false, sendEmail = false) {
      var summaryData;
      if (mockupSwitch) {
        summaryData = JSON.parse(summaryStepMockupData);
      } else {
        summaryData = getSummaryData();
      }

      if (inputValue && sendEmail) {
        return {
          email: inputValue,
          products: getSummaryAllProductsIds(summaryData)
        };
      } else if (inputValue) {
        return {
          message: inputValue,
          products: getSummaryAllProductsIds(summaryData)
        };
      } else {
        return getSummaryAllProductsIds(summaryData);
      }
    };

    init = function (data) {
      app_data.populate(data);
    };

    return {
      init: init,
      state: state,
      populate: populate,
      calculatePrice: calculatePrice,
      setNextStep: setNextStep,
      setPreviousStep: setPreviousStep,
      setLastStep: setLastStep,
      getCurrentStepData: getCurrentStepData,
      getProductFromCurrentStep: getProductFromCurrentStep,
      getProductRelatedItems: getProductRelatedItems,
      addProduct: addProduct,
      addAccessory: addAccessory,
      addRelatedItem: addRelatedItem,
      checkIfProductHasUniqueGroup: checkIfProductHasUniqueGroup,
      stepUpdateGroupsInformation: stepUpdateGroupsInformation,
      deleteGroupsWhenNavigatingToPreviousStep: deleteGroupsWhenNavigatingToPreviousStep,
      isContainerSKUFilter: isContainerSKUFilter,
      isRelatedItemFromGroupAlreadyChosen: isRelatedItemFromGroupAlreadyChosen,
      getSummaryData: getSummaryData,
      findAllAvailableSteps: findAllAvailableSteps,
      findProductsToShowBasedOnDependentIds: findProductsToShowBasedOnDependentIds,
      checkIfNextStepIsValid: checkIfNextStepIsValid,
      checkIfNextStepIsValidBasedOnPurpose: checkIfNextStepIsValidBasedOnPurpose,
      checkForNextValidStep: checkForNextValidStep,
      checkIfStepIsValid: checkIfStepIsValid,
      clearProductsWithRelatedAndAccessories: clearProductsWithRelatedAndAccessories,
      emptyProductCheck: emptyProductCheck,
      getSummaryDataToSend: getSummaryDataToSend,
      isProductAlreadyChosenInPreviousSteps: isProductAlreadyChosenInPreviousSteps,
      getAllChoicesSKUArray: getAllChoicesSKUArray
    };
  })();
});

require([
    "jquery"
], function ($) {
    $(document).ready(function () {
        app_data.init(stepsJsonData);
        templates.init($('#configurator-app'));
        app_actions.init();
        helpers.mobileBodyClasses();
    });
});

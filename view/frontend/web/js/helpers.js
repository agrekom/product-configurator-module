require(['jquery'], function ($) {
  helpers = (function () {
    var idSelectorClone,
      consoleInfoColor,
      consoleInfoColor2,
      consoleProductAddedColor,
      consoleProductRemovedColor,
      selectFromApp,
      delegateClickToContainer,
      isMobile,
      mobileBodyClasses,
      sliderTogglerForDevices,
      destroyOwlSlider,
      initializeOwlSlider,
      mergeAndDeduplicateArrays,
      deduplicateArray,
      displayAlert,
      scrollToTop,
      findArraysIntersection;

    idSelectorClone = function (name) {
      var id = $('#' + name).attr('id');
      return $('#' + name)
        .clone()
        .removeAttr('id')
        .addClass('js-' + id)
        .addClass('container-for-' + id);
    };

    consoleInfoColor = function () {
      return 'display: inline-block; padding: 6px; background-color: DarkCyan';
    };

    consoleInfoColor2 = function () {
      return 'display: inline-block; padding: 6px; background-color: DeepSkyBlue; color: MidnightBlue;';
    };

    consoleProductAddedColor = function () {
      return 'display: inline-block; padding: 6px; background-color: ForestGreen';
    };

    consoleProductRemovedColor = function () {
      return 'display: inline-block; padding: 6px; background-color: DarkRed';
    };

    delegateClickToContainer = function (
      className,
      functionToUse,
      stopPropagation = false
    ) {
      $('body').on('click', '.' + className, function (event) {
        if (stopPropagation) {
          event.stopPropagation();
        }
        var $targetElem = $(event.target),
          $containerToReach;

        if ($targetElem.hasClass(className)) {
          $containerToReach = $targetElem;
        } else {
          $containerToReach = $targetElem.closest('.' + className);
        }
        functionToUse($containerToReach);
      });
    };

    selectFromApp = function (selector) {
      return $('#configurator-app ' + selector);
    };

    isMobile = function () {
      return window.matchMedia('(max-width: 767px)').matches;
    };

    mobileBodyClasses = function () {
      if (isMobile()) {
        $('body').addClass('is-mobile');
        $('body').removeClass('is-desktop');
      } else {
        $('body').removeClass('is-mobile');
        $('body').addClass('is-desktop');
      }
    };

    sliderTogglerForDevices = function () {
      mobileBodyClasses();
      if (!isMobile()) {
        initializeOwlSlider('.js-step-products');
        // initializeOwlSlider('.js-step-accessories');
      } else {
        destroyOwlSlider('.js-step-products');
        // destroyOwlSlider('.js-step-accessories');
      }
    };

    destroyOwlSlider = function (selector) {
      if ($(selector + '.owl-carousel.owl-loaded').length > 0) {
        selectFromApp(selector)
          .trigger('destroy.owl.carousel')
          .removeClass('owl-loaded');
        selectFromApp(selector).find('.owl-stage-outer').children().unwrap();
        console.info(
          '%cslider destroyed: ' + selector,
          'display: inline-block; padding: 6px; background-color: DarkRed'
        );
      }
    };

    initializeOwlSlider = function (selector) {
      if (
        !isMobile() &&
        $(selector + '.owl-carousel.owl-loaded').length === 0 &&
        $(selector + '.slider-to-be-initialized').length > 0
      ) {
        selectFromApp(selector).owlCarousel({
          loop: false,
          dots: false,
          nav: true,
          margin: 0,
          navText: [
            '<span class="chevron left"></span>',
            '<span class="chevron right"></span>'
          ],
          responsive: {
            0: {
              items: 1
            },
            768: {
              items: 3
            }
          }
        });
        console.info(
          '%cslider initialized for: ' + selector,
          'display: inline-block; padding: 6px; background-color: OrangeRed'
        );
      }
    };

    mergeAndDeduplicateArrays = function (array1, array2) {
      return (array1 = Array.from(new Set(array1.concat(array2))));
    };

    deduplicateArray = function (array) {
      return Array.from(new Set(array));
    };

    displayAlert = function (msg) {
      var $msgElement = $(
          `<div class="alert-content"><span class="alert-msg">${msg}</span></div>`
        ),
        $closeBtn = $(
          `<button class="js-alert-close alert-close close"></button>`
        ),
        $infoIcon = $('<span class="info-icon">&#8505;</span>'),
        $alertElement = helpers.selectFromApp('.js-alert');

      $alertElement.html('');
      $alertElement.append($msgElement);
      $alertElement.append($infoIcon);
      $alertElement.append($closeBtn);
      $alertElement.removeClass('is-hidden');
    };

    scrollToTop = function () {
      document.body.scrollTop = 0; // For Safari
      document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    };

    /**
     * Beautiful reduce for finding intersection for multiple arrays
     */
    findArraysIntersection = function (arrayOfArrays) {
      return arrayOfArrays.reduce((a, b) => a.filter((c) => b.includes(c)));
    };

    return {
      idSelectorClone: idSelectorClone,
      consoleInfoColor: consoleInfoColor,
      consoleInfoColor2: consoleInfoColor2,
      consoleProductAddedColor: consoleProductAddedColor,
      consoleProductRemovedColor: consoleProductRemovedColor,
      selectFromApp: selectFromApp,
      delegateClickToContainer: delegateClickToContainer,
      isMobile: isMobile,
      mobileBodyClasses: mobileBodyClasses,
      sliderTogglerForDevices: sliderTogglerForDevices,
      destroyOwlSlider: destroyOwlSlider,
      initializeOwlSlider: initializeOwlSlider,
      mergeAndDeduplicateArrays: mergeAndDeduplicateArrays,
      deduplicateArray: deduplicateArray,
      displayAlert: displayAlert,
      scrollToTop: scrollToTop,
      findArraysIntersection: findArraysIntersection
    };
  })();
});

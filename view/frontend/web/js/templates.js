require(['jquery'], function ($) {
  templates = (function () {
    var init,
      $configuratorApp,
      updatePurposeDisplay,
      updateCurrentStep,
      updatePriceDisplay,
      updateProductsDisplay,
      updateAttributesHeight,
      updateItemsImg,
      updateStepsNumber,
      showCurrentStep,
      showProductAccessories,
      showProductRelatedItems,
      clearProductsWithRelatedAndAccessories;

    updateCurrentStep = function (settings) {
      updateProductsDisplay(settings);
      updatePriceDisplay();
      if (app_data.state.currentStep.name === 'step_10') {
        updatePurposeDisplay();
      }
    };

    updatePurposeDisplay = function () {
      var chosenPurposeProducts = Object.values(
          app_data.state.choices.step_10.products
        ),
        dependentPurposeIdsArray = [];

      if (chosenPurposeProducts.length > 0) {
        chosenPurposeProducts.forEach((purpose) => {
          dependentPurposeIdsArray.push([
            ...Object.values(purpose.dependent_product_ids),
            purpose.id
          ]);
        });

        /* Intersection of arrays for dependent purposes */
        dependentPurposeIdsArray = helpers.findArraysIntersection(
          dependentPurposeIdsArray
        );

        var $allPurposeElements = helpers.selectFromApp('.js-add-product');
        $allPurposeElements.addClass('is-disabled');

        var allPurposeItems = app_data.getCurrentStepData().step_products;

        Object.values(allPurposeItems).forEach((purposeItem) => {
          dependentPurposeIdsArray.forEach((dependentId) => {
            if (purposeItem.id === dependentId) {
              var $available = helpers.selectFromApp(
                '.js-add-product[data-product-id="' +
                  Number.parseInt(dependentId) +
                  '"]'
              );
              $available.removeClass('is-disabled');
              $available.addClass('js-add-product');
            }
          });
        });

        helpers
          .selectFromApp('.js-add-product[data-product-id].is-disabled')
          .removeClass('js-add-product');
      }
    };

    updatePriceDisplay = function () {
      var newCalculatedPrice = app_data.calculatePrice(),
        newPrice = '';

      newPrice = new Intl.NumberFormat(translations.languageShortCode, {
        style: 'currency',
        currency: translations.currencyCode
      }).format(newCalculatedPrice);

      helpers
        .selectFromApp('.js-calculated-price .js-price-sum')
        .html(newPrice);
    };

    updateProductsDisplay = function (settings = {}) {
      var isClicked = false,
        previousStep = false;
      if (settings.clicked) {
        isClicked = true;
      }
      if (settings.previousStep) {
        previousStep = true;
      }
      var currentStepProducts =
          app_data.state.choices[app_data.state.currentStep.name].products,
        currentStepAccessories =
          app_data.state.choices[app_data.state.currentStep.name].accessories,
        currentStepRelated = Object.values(
          app_data.state.choices[app_data.state.currentStep.name].related
        ),
        classNameToUse = 'is-chosen',
        classNameChecked = 'is-checked';

      console.log(currentStepRelated);

      helpers.selectFromApp('.js-add-product').removeClass(classNameToUse);
      helpers
        .selectFromApp('.js-add-product .js-custom-checkbox')
        .removeClass(classNameChecked);
      helpers.selectFromApp('.js-add-accessory').removeClass(classNameToUse);

      if (Array.isArray(currentStepProducts)) {
        currentStepProducts.forEach(function (product) {
          var $productElement = helpers.selectFromApp(
            '.js-add-product[data-product-id="' + product.id + '"]'
          );
          $productElement.addClass(classNameToUse);
          $productElement
            .find('.js-custom-checkbox:not(.js-add-accessory-mobile)')
            .addClass(classNameChecked);
        });
      }

      if (Array.isArray(currentStepAccessories)) {
        currentStepAccessories.forEach(function (product) {
          var $accessoryItem = helpers.selectFromApp(
            '.js-add-accessory[data-product-id="' + product.id + '"]'
          );
          $accessoryItem.addClass(classNameToUse);
          $accessoryItem.find('.js-custom-checkbox').addClass(classNameChecked);
        });
      }

      if (
        Array.isArray(currentStepProducts) &&
        Array.isArray(currentStepRelated)
      ) {
        var selectedId = 0;

        if (currentStepRelated.length > 0) {
          selectedId = currentStepRelated[0][0].id;
        }

        if (app_data.state.currentStep.name !== 'step_10') {
          currentStepProducts.forEach(function (product) {
            var allChoices = Object.values(app_data.state.choices);

            allChoices.forEach((stepChoice) => {
              stepChoice.products.forEach((choosenProduct) => {
                if (choosenProduct.id === product.id) {
                  var productRelatedItems = app_data.getProductRelatedItems(
                    product.id
                  );
                  if (isClicked || previousStep) {
                    showProductRelatedItems(
                      productRelatedItems,
                      product.id,
                      selectedId
                    );
                  }
                }
              });
            });
          });
        }

        /* calculations just for debbuging */
        app_data.getAllChoicesSKUArray(true);
      }

      if (currentStepRelated.length === 0) {
        helpers.selectFromApp('.js-left-column-title').hide();
        helpers.selectFromApp('.js-footer-left-column').addClass('hidden');
        helpers.selectFromApp('.js-footer-overlay').addClass('hidden');

        view_constructor.buildView.clearProductRelatedItems();
      } else {
        helpers
          .selectFromApp('.js-add-product')
          .addClass('show-change-related-button');
      }

      updateStepsNumber();
    };

    updateAttributesHeight = function () {
      if (helpers.selectFromApp('.js-step-attribute-codes').length > 0) {
        var attrFound = helpers.selectFromApp('.js-step-attribute-codes')[0]
          .children.length;

        var $attrToCheck = helpers.selectFromApp(
          '.js-compare-table .js-step-attribute-codes'
        );

        var $itemsToCheck = helpers.selectFromApp(
          '.js-compare-table .js-step-products-mobile'
        );

        /* find highest elements in table */
        for (var i = 0; i < attrFound; i++) {
          var rowHeight = 0,
            selector = '[data-attr-index="' + i + '"]';

          rowHeight = $attrToCheck.find(selector).height();

          $itemsToCheck.find(selector).each(function (index, item) {
            var $item = $(item);

            if ($item.height() > rowHeight) {
              rowHeight = $item.height();
            }
          });

          /* adding some padding */
          if (rowHeight > 40) {
            rowHeight = rowHeight + 20;
          }

          /* set heights of elements in table */
          $attrToCheck.find(selector).each(function (index, item) {
            var $item = $(item);
            $item.height(rowHeight);
          });

          $itemsToCheck.find(selector).each(function (index, item) {
            var $item = $(item);
            $item.height(rowHeight);
          });
        }
      }
    };

    updateItemsImg = function () {
      view_constructor.buildView.relatedItemsImg();
    };

    updateStepsNumber = function () {
      view_constructor.buildView.updateStepsNumber();
    };

    clearProductsWithRelatedAndAccessories = function () {
      view_constructor.buildView.clearProductsWithRelatedAndAccessories();
      showCurrentStep();
    };

    showCurrentStep = function (settings) {
      $configuratorApp.html(
        helpers.idSelectorClone(app_data.state.currentStep.name)
      );
      if (app_data.getCurrentStepData().step_page_layout === 'purpose') {
        view_constructor.buildView.purpose();
      } else if (app_data.getCurrentStepData().step_page_layout === 'summary') {
        view_constructor.buildView.summary();
      } else {
        view_constructor.buildView.commonChoicesLayout();
      }

      if (app_data.getCurrentStepData().step_products) {
        var stepProducts = app_data.getCurrentStepData().step_products,
          productsToShow =
            app_data.findProductsToShowBasedOnDependentIds(stepProducts);

        if (productsToShow.length > 3) {
          /*
           * If we are displaying more than 3 products or accessories
           * slider will be activated, to recognize this later
           * class 'slider-to-be-initialized' is addded
           */
          helpers
            .selectFromApp('.js-step-products')
            .addClass('slider-to-be-initialized');
          helpers.initializeOwlSlider('.js-step-products');
        }

        var checkForChosenProducts =
          app_data.state.choices[app_data.state.currentStep.name].products;
        if (checkForChosenProducts.length > 0) {
          checkForChosenProducts.forEach((product) => {
            showProductAccessories(product.id);
          });
        }

        updateProductsDisplay(settings);
      }

      updatePriceDisplay();

      if (app_data.state.currentStep.name === 'step_10') {
        updatePurposeDisplay();
      }
    };

    showProductAccessories = function (productId) {
      if (productId) {
        var product = app_data.getProductFromCurrentStep(productId),
          productAccessoriesArray = Object.values(product.product_accessories);

        if (productAccessoriesArray.length) {
          helpers
            .selectFromApp('.js-footer-content-column')
            .removeClass('hidden');
          helpers.selectFromApp('.js-step-footer-title').show();
          view_constructor.buildView.productAccessories(
            productAccessoriesArray,
            productId
          );
          view_constructor.buildView.productAccessoriesMobile(
            productAccessoriesArray,
            productId
          );

          // if (productAccessoriesArray.length > 3) {
          //   /* reinit owl carousel */
          //   helpers.destroyOwlSlider('.js-step-accessories');

          //   helpers
          //     .selectFromApp('.js-step-accessories')
          //     .addClass('slider-to-be-initialized');
          //   helpers.initializeOwlSlider('.js-step-accessories');
          // } else {
          //   helpers
          //     .selectFromApp('.js-step-accessories')
          //     .removeClass('slider-to-be-initialized');
          //   helpers.destroyOwlSlider('.js-step-accessories');
          // }
        } else {
          helpers.selectFromApp('.js-footer-content-column').addClass('hidden');
          helpers.selectFromApp('.js-step-footer-title').hide();
          view_constructor.buildView.clearProductAccessories();
        }
      } else {
        helpers.selectFromApp('.js-footer-content-column').addClass('hidden');
        helpers.selectFromApp('.js-step-footer-title').hide();
        view_constructor.buildView.productAccessories([], productId);
      }
    };

    showProductRelatedItems = function (
      relatedItemsArray,
      productId,
      selectedId = 0
    ) {
      /**
       * first page layouts will not have raleted products displayed
       */
      var pageLayout = app_data.getCurrentStepData().step_page_layout;
      if (
        pageLayout === 'single_choice_with_config' ||
        pageLayout === 'single_choice_not_obligatory_with_more_extensive_config'
      ) {
        return;
      }

      /**
       * checking if there are any groups with chosen related items
       * in step ahead (when using previous button)
       */
      app_data.deleteGroupsWhenNavigatingToPreviousStep();

      /**
       * filtering related items based on is_cointer_product in choices
       */
      relatedItemsArray = app_data.isContainerSKUFilter(
        relatedItemsArray,
        true
      );

      /**
       * if product have related items
       * and no related items in the group were selected (can be changed for no_group)
       */
      if (
        relatedItemsArray &&
        !app_data.isRelatedItemFromGroupAlreadyChosen(productId, false)
      ) {
        helpers.selectFromApp('.js-left-column-title').show();
        helpers.selectFromApp('.js-footer-left-column').removeClass('hidden');
        helpers.selectFromApp('.js-footer-overlay').removeClass('hidden');

        view_constructor.buildView.productRelatedItems(
          relatedItemsArray,
          productId,
          selectedId
        );
      }
    };

    init = function ($appElement) {
      $configuratorApp = $appElement;
      /* app data must be initialized first */
      templates.showCurrentStep();
    };

    return {
      init: init,
      showCurrentStep: showCurrentStep,
      updateCurrentStep: updateCurrentStep,
      updatePurposeDisplay: updatePurposeDisplay,
      updateAttributesHeight: updateAttributesHeight,
      updateItemsImg: updateItemsImg,
      updatePriceDisplay: updatePriceDisplay,
      showProductAccessories: showProductAccessories,
      showProductRelatedItems: showProductRelatedItems,
      clearProductsWithRelatedAndAccessories:
        clearProductsWithRelatedAndAccessories
    };
  })();
});

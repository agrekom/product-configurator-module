require(['jquery'], function ($) {
  view_constructor = (function () {
    var MSG,
      buildView,
      buildComponents,
      customCheckboxMobile,
      customRadioMobile,
      imageString,
      buttonChoose;

    buildView = {};
    buildComponents = {};

    MSG = {
      success: 'build successful',
      failed: 'build failed'
    };

    buildView.purpose = function () {
      buildComponents.stepNumberHeader();
      buildComponents.stepHeading();
      buildComponents.calculatedPrice();

      return MSG.success;
    };

    buildView.summary = function () {
      buildComponents.stepNumberHeader();
      buildComponents.summaryProducts();
      buildComponents.calculatedPrice();

      return MSG.success;
    };

    /*
     * For now only need to use common view,
     * for all steps except purpose and summary
     */
    buildView.commonChoicesLayout = function () {
      var stepData = app_data.getCurrentStepData(),
        stepProducts = stepData.step_products,
        productsToShow =
          app_data.findProductsToShowBasedOnDependentIds(stepProducts);

      buildComponents.stepNumberHeader();
      buildComponents.stepHeading();

      buildComponents.products(productsToShow);
      buildComponents.attributeCodes();
      buildComponents.calculatedPrice();

      return true;
    };

    buildView.productAccessories = function (accessoriesArray, productId) {
      buildComponents.accessories(accessoriesArray, productId);
    };

    buildView.productAccessoriesMobile = function (
      accessoriesArray,
      productId
    ) {
      buildComponents.accessoriesMobile(accessoriesArray, productId);
    };

    buildView.productRelatedItems = function (
      relatedItemsArray,
      productId,
      selectedId = 0
    ) {
      helpers
        .selectFromApp('.js-related-items-mobile-title')
        .removeClass('is-visible');
      buildComponents.relatedItemsSelect(
        relatedItemsArray,
        productId,
        selectedId
      );
      helpers
        .selectFromApp(
          `.js-add-product[data-product-id="${productId}"] .js-related-items-mobile-title`
        )
        .addClass('is-visible');
      if (relatedItemsArray.length > 0) {
        buildComponents.relatedItemsImg();
      } else {
        helpers.selectFromApp('.js-left-column-title').show();
        helpers.selectFromApp('.js-footer-left-column').removeClass('hidden');
        helpers.selectFromApp('.js-footer-overlay').removeClass('hidden');
      }
    };

    buildView.relatedItemsImg = function () {
      buildComponents.relatedItemsImg();
    };

    buildView.updateStepsNumber = function () {
      buildComponents.stepNumberHeader();
    };

    buildView.clearProductAccessories = function () {
      helpers.selectFromApp('.js-step-footer-title').hide();
      helpers.selectFromApp('.js-add-product .js-accessories-mobile').empty();
      helpers.selectFromApp('.js-step-accessories').empty();
    };

    buildView.clearProductRelatedItems = function () {
      helpers.selectFromApp('.js-left-column-title').hide();
      helpers.selectFromApp('.js-footer-left-column').addClass('hidden');
      helpers.selectFromApp('.js-footer-overlay').addClass('hidden');
      helpers
        .selectFromApp('.js-related-items-mobile-title')
        .removeClass('is-visible');
      helpers.selectFromApp('.js-related-item-img-holder').empty();
      helpers.selectFromApp('.js-add-product .js-related-items-mobile').empty();
      helpers.selectFromApp('.js-related-items').empty();
    };

    buildView.clearProductsWithRelatedAndAccessories = function () {
      helpers.selectFromApp('.js-step-products').empty();
      helpers.selectFromApp('.js-step-products-mobile').empty();
      helpers.selectFromApp('.js-step-footer-title').hide();
      helpers.selectFromApp('.js-step-accessories').empty();
      helpers.selectFromApp('.js-left-column-title').hide();
      helpers.selectFromApp('.js-footer-left-column').addClass('hidden');
      helpers.selectFromApp('.js-footer-overlay').addClass('hidden');
      helpers
        .selectFromApp('.js-related-items-mobile-title')
        .removeClass('is-visible');
      helpers.selectFromApp('.js-related-item-img-holder').empty();
      helpers.selectFromApp('.js-related-items').empty();
    };

    buildComponents.stepNumberHeader = function () {
      var elements = '',
        index = app_data.state.stepMarker,
        availableStepsNumber = app_data.findAllAvailableSteps(),
        allStepsNr =
          availableStepsNumber > 2
            ? availableStepsNumber
            : app_data.state.steps.length;

      elements += '<h3>' + translations.step + '</h3>';
      elements += '<p>' + index + '/' + allStepsNr + '</p>';

      helpers.selectFromApp('.js-step-number-header').html(elements);
    };

    buildComponents.stepHeading = function () {
      helpers
        .selectFromApp('.js-current-step-heading')
        .html(app_data.getCurrentStepData().step_title);
    };

    buildComponents.calculatedPrice = function () {
      var elements = '';

      elements += '<p>' + translations.sum + ':</p>';
      elements += '<div class="js-price-sum">' + '0,00' + 'zł' + '</div>';
      elements += '<p>' + translations.vatInfo + '</p>';

      helpers.selectFromApp('.js-calculated-price').html(elements);
    };

    buildComponents.products = function (products) {
      buildView.clearProductsWithRelatedAndAccessories();
      if (products.length > 0) {
        products.forEach((product) => {
          helpers
            .selectFromApp('.js-step-products')
            .append(buildComponents.singleProduct(product));
          helpers
            .selectFromApp('.js-step-products-mobile')
            .append(buildComponents.singleProductCompareTable(product, true));
        });
      }

      if (Boolean(app_data.getCurrentStepData().comparison_table_title)) {
        var comparisonTableTitle =
          app_data.getCurrentStepData().comparison_table_title;

        if (comparisonTableTitle === 'technical_specifications') {
          helpers
            .selectFromApp('.js-show-compare-label')
            .html(translations.technicalSpecificationsBtn);
          helpers
            .selectFromApp('.js-compare-table-title')
            .html(translations.technicalSpecificationsTitle);
        } else {
          helpers
            .selectFromApp('.js-show-compare-label')
            .html(translations.comparisonTableBtn);
          helpers
            .selectFromApp('.js-compare-table-title')
            .html(translations.comparisonTableTitle);
        }
      }
    };

    buildComponents.singleProduct = function (product) {
      var $productElement = $(
          '<div class="product-item js-add-product"></div>'
        ),
        isProductAlreadyChosen = app_data.isProductAlreadyChosenInPreviousSteps(
          product.id
        );

      if (isProductAlreadyChosen) {
        $productElement
          .addClass('is-already-chosen')
          .removeClass('js-add-product');
      }

      $productElement.attr('data-product-id', product.id);

      if (isProductAlreadyChosen) {
        $productElement.append(
          '<div class="product-item-radio is-chosen">' +
            customRadioMobile() +
            '</div>'
        );
      } else {
        $productElement.append(
          '<div class="product-item-radio">' + customRadioMobile() + '</div>'
        );
      }

      $productElement.append(
        $(
          '<div class="product-item-img">' +
            imageString(product.image, product.name) +
            '</div>'
        )
      );

      var $productElementInformation = $(
        '<div class="product-item-information"></div>'
      );
      $productElementInformation.append(
        $('<div class="product-item-name">' + product.name + '</div>')
      );

      if (product.short_desc) {
        $productElementInformation.append(
          $(
            '<div class="product-item-description">' +
              product.short_desc +
              '</div>'
          )
        );
      }
      $productElementInformation.append(
        $(
          '<div class="product-item-price"> +' +
            product.price_with_tax +
            '</div>'
        )
      );

      $productElement.append($productElementInformation);

      if (isProductAlreadyChosen) {
        $productElement.append(
          $(
            `<div class="product-item-already-chosen">${translations.productAlreadyChosen}</div>`
          )
        );
      } else {
        $productElement.append(
          $(
            '<div class="product-item-button">' +
              buttonChoose() +
              buttonChangeRelated() +
              '</div>'
          )
        );
      }
      $productElement.append(
        $(
          '<div class="js-related-items-mobile-title related-items-mobile-title"></div>'
        )
      );
      $productElement.append(
        $('<div class="js-related-items-mobile related-items-content"></div>')
      );
      $productElement.append(
        $(
          '<div class="js-accessories-mobile product-item-accessories-mobile"></div>'
        )
      );

      if (product.attributes !== null) {
        $productElement.append(
          buildComponents.singleProductAttributes(product)
        );
      } else {
        $productElement.addClass('no-attributes');
      }

      return $productElement;
    };

    buildComponents.singleProductCompareTable = function (product) {
      var $productElement = $('<div class="product-item"></div>');

      $productElement.attr('data-product-id', product.id);
      $productElement.append(
        $(
          '<div class="product-item-img">' +
            imageString(product.image, product.name) +
            '</div>'
        )
      );

      if (product.attributes !== null) {
        $productElement.append(
          buildComponents.singleProductAttributes(product)
        );
      } else {
        $productElement.addClass('no-attributes');
      }

      return $productElement;
    };

    buildComponents.singleProductAttributes = function (product) {
      var $elements = $('<div class="product-item-attributes"></div>'),
        prodAttrEntries = Object.entries(product.attributes),
        stepDataAttrKeys = Object.keys(
          app_data.getCurrentStepData().step_attribute_codes
        );

      /* finding attributes for product based on step global attributes */
      var attrIndex = 0;
      stepDataAttrKeys.forEach((stepAttributeCode) => {
        var foundAttr = false,
          foundValue = '';

        prodAttrEntries.forEach(([attrCode, attribute]) => {
          if (attrCode == stepAttributeCode) {
            foundAttr = true;
            foundValue = attribute.value;
          }
        });

        if (foundAttr) {
          $elements.append(
            $(
              '<div class="attribute-item" data-attr-index=' +
                attrIndex +
                '>' +
                foundValue +
                '</div>'
            )
          );
          attrIndex++;
        } else {
          $elements.append('<div class="attribute-item is-empty"></div>');
        }
      });

      return $elements;
    };

    buildComponents.attributeCodes = function () {
      var stepData = app_data.getCurrentStepData(),
        stepAttributeCodes = stepData.step_attribute_codes,
        stepProducts = stepData.step_products,
        productsToShow =
          app_data.findProductsToShowBasedOnDependentIds(stepProducts),
        elements = '',
        areThereAnyProductsWithAttributes = false;

      if (productsToShow.length > 0) {
        productsToShow.forEach((product) => {
          if (product.attributes !== null) {
            areThereAnyProductsWithAttributes = true;
          }
        });
      }

      if (stepAttributeCodes && areThereAnyProductsWithAttributes) {
        var attrIndex = 0;

        Object.values(stepAttributeCodes).forEach((code) => {
          var attributeValuesFoundInProducts = false;

          productsToShow.forEach((product) => {
            if (product.attributes) {
              Object.values(product.attributes).forEach((attribute) => {
                if (attribute.label === code) {
                  attributeValuesFoundInProducts = true;
                }
              });
            }
          });

          if (attributeValuesFoundInProducts) {
            elements +=
              '<div class="attribute-item" data-attr-index=' +
              attrIndex +
              '><p>' +
              code +
              '</p></div>';
            attrIndex++;
          }
        });
        helpers.selectFromApp('.js-step-attribute-codes').html($(elements));
      } else {
        helpers.selectFromApp('.js-step-attribute-codes').empty();
      }
    };

    buildComponents.accessories = function (accessoriesArray, productId) {
      var elements = '';

      if (accessoriesArray.length > 0) {
        accessoriesArray.forEach((accessory) => {
          elements +=
            '<div class="js-add-accessory accessory-item" data-parent-id="' +
            productId +
            '" data-product-id="' +
            accessory.id +
            '">';
          elements +=
            '<div class="accessory-item-img">' +
            imageString(accessory.image, accessory.name) +
            '</div>';
          elements +=
            '<div class="accessory-item-title"><p>' +
            accessory.name +
            '</p></div>';
          if (accessory.short_desc) {
            elements +=
              '<div class="accessory-item-description"><p>' +
              accessory.short_desc +
              '</p></div>';
          }
          elements += '<div class="accessory-item-footer">';
          elements +=
            '<div class="accessory-item-price"><p>+' +
            accessory.price_with_tax +
            '</p></div>';
          elements +=
            '<div class="accessory-item-button">' +
            customCheckbox() +
            buttonChoose() +
            '</div>';
          elements += '</div>';
          elements += '</div>';
        });
      }

      helpers.selectFromApp('.js-step-accessories').html($(elements));
    };

    buildComponents.accessoriesMobile = function (accessoriesArray, productId) {
      var elements =
        '<div class="additional-accessories-title">' +
        translations.additional_accessories +
        '</div>';

      if (accessoriesArray.length > 0) {
        accessoriesArray.forEach((accessory) => {
          elements +=
            '<div class="js-add-accessory accessory-item-mobile" data-parent-id="' +
            productId +
            '" data-product-id="' +
            accessory.id +
            '">';
          elements +=
            '<div class="accessory-item-mobile-button">' +
            customCheckboxMobile() +
            '</div>';
          elements +=
            '<div class="accessory-item-mobile-title"><p>' +
            accessory.name +
            '</p></div>';
          if (accessory.short_desc) {
            elements +=
              '<div class="accessory-item-mobile-description"><p>' +
              accessory.short_desc +
              '</p></div>';
          }
          elements += '</div>';
        });
      }

      helpers.selectFromApp('.js-add-product .js-accessories-mobile').empty();
      helpers
        .selectFromApp(
          '.js-add-product[data-product-id="' +
            productId +
            '"] .js-accessories-mobile'
        )
        .html(elements);
    };

    buildComponents.relatedItemsSelect = function (
      relatedItemsArray,
      productId,
      selectedId = 0
    ) {
      if (productId) {
        var elements = '',
          $select = $(
            `<select data-parent-id="${productId}" class="js-related-items-select related-items-select"></select>`
          );

        if (relatedItemsArray.length > 0) {
          relatedItemsArray.forEach((relatedItem, index) => {
            var label = relatedItem.short_desc
                ? relatedItem.short_desc
                : relatedItem.name,
              name = label + ' +' + relatedItem.price_with_tax,
              img = relatedItem.image,
              id = relatedItem.id,
              selected = '';

            if (selectedId !== 0 && selectedId === id) {
              selected = 'selected';
            } else if (selectedId === 0 && index === 0) {
              selected = 'selected';
            }

            elements += `<option value="${id}" data-item-image="${img}" ${selected}>${name}</option>`;
          });
          $select.append($(elements));
          var $selectMobile = $select
            .clone()
            .removeClass('js-related-items-select')
            .addClass('js-related-items-select-mobile')
            .addClass('is-mobile');

          helpers.selectFromApp('.js-related-items').empty();
          helpers.selectFromApp('.js-related-items').append($select);
          helpers.selectFromApp('.js-related-items-mobile').empty();
          helpers
            .selectFromApp('.js-related-items-mobile-title')
            .html(translations.relatedItemsMobileTitle);
          helpers
            .selectFromApp(
              '.js-add-product[data-product-id="' +
                productId +
                '"] .js-related-items-mobile'
            )
            .append($selectMobile);
        } else {
          helpers.selectFromApp('.js-related-items').empty();
          helpers.selectFromApp('.js-related-items-mobile-title').empty();
          helpers.selectFromApp('.js-related-items-mobile').empty();
        }
      } else {
        helpers.selectFromApp('.js-related-items').empty();
        helpers.selectFromApp('.js-related-items-mobile-title').empty();
        helpers.selectFromApp('.js-related-items-mobile').empty();
      }
    };

    buildComponents.relatedItemsImg = function () {
      var optionSelected = helpers.selectFromApp(
          '.js-related-items-select option:selected'
        ),
        imgUrl = optionSelected.attr('data-item-image'),
        selector = helpers.selectFromApp('.js-related-item-img-holder'),
        altText = optionSelected.text();

      if (selector.find(img).length > 0) {
        selector.find(img).attr('src', imgUrl).attr('alt', altText);
      } else {
        var img = `<img src="${imgUrl}" alt="${altText}"/>`;
        selector.html(img);
      }
    };

    buildComponents.summaryProducts = function () {
      var choices;
      if (mockupSwitch) {
        choices = JSON.parse(summaryStepMockupData);
      } else {
        choices = app_data.state.choices;
      }

      Object.entries(choices).forEach(([stepName, choice]) => {
        if (
          stepName === 'step_10' ||
          stepName === 'summary_step' ||
          choice.products.length === 0
        ) {
          return;
        }

        if (app_data.emptyProductCheck(choice.products)) {
          return;
        }

        var $summaryProduct = $(
          `<div data-chosen-on-step="${stepName}" class="js-summary-item summary-item"></div>`
        );

        choice.products.forEach((product) => {
          var $summarySingleProduct = $(
              '<div class="summary-single-product"></div>'
            ),
            singleProduct = '';

          singleProduct +=
            '<div class="summary-item-img">' +
            imageString(product.image, product.name) +
            '</div>';
          singleProduct += `<div class="summary-item-description"><p class="summary-item-name">${product.name}</p></div>`;
          singleProduct += `<div class="summary-item-price">+${product.price_with_tax}</div>`;

          $summarySingleProduct.append(singleProduct);
          $summaryProduct.append($summarySingleProduct);
        });

        if (choice.accessories.length > 0) {
          $summaryProduct.append(
            `<p class="summary-item-subtitle">${translations.accessories}:</p>`
          );
        }

        if (choice.accessories.length > 0) {
          choice.accessories.forEach((accessory) => {
            var $summarySingleProduct = $(
                '<div class="summary-single-product is-accessory"></div>'
              ),
              singleProduct = '';

            singleProduct +=
              '<div class="summary-item-img">' +
              imageString(accessory.image, accessory.name) +
              '</div>';

            var shortDescription = '';
            if (accessory.short_desc) {
              shortDescription += `<p class="summary-item-description-short">${accessory.short_desc}</p>`;
            }
            singleProduct += `<div class="summary-item-description"><p class="summary-item-name">${accessory.name}</p>${shortDescription}</div>`;
            singleProduct += `<div class="summary-item-price">+${accessory.price_with_tax}</div>`;

            $summarySingleProduct.append(singleProduct);
            $summaryProduct.append($summarySingleProduct);
          });
        }

        if (Object.values(choice.related).length > 0) {
          $summaryProduct.append(
            `<p class="summary-item-subtitle">${translations.related}:</p>`
          );
        }

        if (Object.values(choice.related).length > 0) {
          Object.values(choice.related).forEach((relatedItems) => {
            var $summarySingleProduct = $(
                '<div class="summary-single-product is-accessory"></div>'
              ),
              item = relatedItems[0],
              singleProduct = '';

            singleProduct +=
              '<div class="summary-item-img">' +
              imageString(item.image, item.name) +
              '</div>';

            var shortDescription = '';
            if (item.short_desc) {
              shortDescription += `<p class="summary-item-description-short">${item.short_desc}</p>`;
            }
            singleProduct += `<div class="summary-item-description"><p class="summary-item-name">${item.name}</p>${shortDescription}</div>`;
            singleProduct += `<div class="summary-item-price">+${item.price_with_tax}</div>`;

            $summarySingleProduct.append(singleProduct);
            $summaryProduct.append($summarySingleProduct);
          });
        }

        choice.products.forEach((product) => {
          var $summarySingleProduct = $(
              '<div class="summary-products-price-mobile"></div>'
            ),
            singleProduct = '';

          singleProduct += `<div class="summary-item-price">+${product.price_with_tax}</div>`;

          $summarySingleProduct.append(singleProduct);
          $summaryProduct.append($summarySingleProduct);
        });

        helpers.selectFromApp('.js-summary-products').append($summaryProduct);
      });
    };

    customCheckboxMobile = function () {
      return '<div class="js-custom-checkbox js-add-accessory-mobile add-accessory-mobile custom-checkbox"></div>';
    };

    customCheckbox = function () {
      return '<div class="custom-checkbox"></div>';
    };

    customRadioMobile = function () {
      return '<div class="js-custom-radio custom-radio"></div>';
    };

    imageString = function (src, alt) {
      return '<img class="item-image" src="' + src + '" alt="' + alt + '"/>';
    };

    buttonChoose = function () {
      return (
        '<button class="choose-btn" data-text-chosen="' +
        translations.chosen +
        '" data-text-choose="' +
        translations.choose +
        '"></button>'
      );
    };

    buttonChangeRelated = function () {
      return (
        '<button class="js-change-related-btn change-related-btn hidden">' +
        translations.changeRelated +
        '</button>'
      );
    };

    return {
      buildView: buildView
    };
  })();
});
